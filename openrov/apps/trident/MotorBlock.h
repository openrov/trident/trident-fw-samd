#pragma once

// Includes

#include "Motor.h"

class MotorBlock
{
public:
	// Methods
	MotorBlock();

	void Initialize();
	void Post();
	void Update();

private:
	Motor m_portMotor;
	Motor m_vertMotor;
	Motor m_starMotor;

	mavlink_orov_trident_motor_command_t m_command;
	bool m_commandAvailable = false;
	
	orutil::CTimer m_timeoutTimer;
	orutil::CTimer m_controlTimer;
    orutil::CTimer m_zeroTimer;

	void HandleMessages();
};
