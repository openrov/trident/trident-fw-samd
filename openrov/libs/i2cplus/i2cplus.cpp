#include "i2cplus.h"

#include <log.h>
#include <periph/gpio.h>
#include <xtimer.h>
#include <assert.h>

// Helper function for cleaning up result handling
#define HANDLE_RESULT(x) ( static_cast<i2c_retcode_t>( m_results.Add( x ) ) )

I2C::I2C( i2c_t i2cDevIn, i2c_speed_t speedIn )
    : m_i2cDev{ i2cDevIn }
    , m_speed{ speedIn }
{
    // Get the pin numbers
    switch(m_i2cDev) 
    {
    #if I2C_0_EN
        case I2C_0:
        {
            m_pinSDA = I2C_0_SDA;
            m_pinSCL = I2C_0_SCL;
            break;
        }
    #endif
        default:
        {
            // TODO: Need a compile-time check for this
            // Do not allow this
            assert( false );
            return;
        }
    }

    // Calculate toggle period
    if( m_speed == I2C_SPEED_NORMAL )
    {
        // Needs to be at least 10us
        m_togglePeriod_us = 10;
    }
    else if( m_speed == I2C_SPEED_FAST )
    {
        // Needs to be at least 2.5us
        m_togglePeriod_us = 3;
    }
    else
    {
        m_togglePeriod_us = 100;
    }
}

i2c_retcode_t I2C::Enable()
{
    // Can be called from uninitialized
    if( m_state == EI2CState::READY )
    {
        return I2C_RETCODE_ERROR_ALREADY_ENABLED;
    }

    // Initialize I2C peripheral
    i2c_retcode_t ret = HANDLE_RESULT( i2c_init_master( m_i2cDev, m_speed ) );
    if( ret == I2C_RETCODE_SUCCESS )
    {
        m_state = EI2CState::READY;
        return ret;
    }
    else
    {
        m_state = EI2CState::FAILURE;
        return ret;
    }
}

void I2C::Disable()
{
    // Disable the peripheral and reset the state
    i2c_poweroff( m_i2cDev );
    m_state = EI2CState::UNINITIALIZED;
}

i2c_retcode_t I2C::Reset()
{
    // Calling i2c_init_master does idempotently perform a full reset of the peripheral
    i2c_retcode_t ret = HANDLE_RESULT( i2c_init_master( m_i2cDev, m_speed ) );
    if( !ret )
    {
        m_state = EI2CState::FAILURE;
        return ret;
    }

    m_state = EI2CState::READY;
    return ret;
}

i2c_retcode_t I2C::CheckSCL()
{
    // Disable I2C peripheral, if not already
    Disable();

    // Change SCL pin to an input to do an initial check of its state
    gpio_init( m_pinSCL, GPIO_IN );

    if( gpio_read( m_pinSCL ) == 0 )
    {
        return I2C_RETCODE_ERROR_SCL_LATCHED;
    }

    return I2C_RETCODE_SUCCESS;
}

i2c_retcode_t I2C::CheckSDA()
{
    // Disable I2C peripheral, if not already
    Disable();

    // Change SDA pin to an input to do an initial check of its state
    gpio_init( m_pinSDA, GPIO_IN );

    if( gpio_read( m_pinSDA ) == 0 )
    {
        return I2C_RETCODE_ERROR_SDA_LATCHED;
    }

    return I2C_RETCODE_SUCCESS;
}

i2c_retcode_t I2C::CheckForLatching()
{
    auto scl = CheckSCL();
    auto sda = CheckSDA();

    if( scl && sda )
    {
        LOG_DEBUG( "SDASCL LATCH\n" );
        return HANDLE_RESULT( I2C_RETCODE_ERROR_SDASCL_LATCHED );
    }
    else if( scl )
    {
        LOG_DEBUG( "SCL LATCH\n" );
        return HANDLE_RESULT( I2C_RETCODE_ERROR_SCL_LATCHED );
    }
    else if( sda )
    {
        LOG_DEBUG( "SDA LATCH\n" );
        return HANDLE_RESULT( I2C_RETCODE_ERROR_SDA_LATCHED );
    }
    else
    {
        return HANDLE_RESULT( I2C_RETCODE_SUCCESS );
    }
}

i2c_retcode_t I2C::PerformToggleRecovery()
{
    // Perform initial health check
    auto ret = CheckForLatching();
    if( ret == I2C_RETCODE_ERROR_SCL_LATCHED )
    {
        // Failed health check on SCL. Toggle will not fix this, so we abort.
        return ret;
    }

    // Set SDA to input and SCL output
    gpio_init( m_pinSDA, GPIO_IN );
    gpio_init( m_pinSCL, GPIO_OUT );

    // Toggle SCL until SDA is released
    int count = 0;
    while( count < 10 )
    {
        // Toggle SCL 16 times to force any slaves to release their hold on SDA
        for( int i = 0; i < 16; ++i )
        {
            gpio_clear( m_pinSCL );
            xtimer_usleep( m_togglePeriod_us );
            gpio_set( m_pinSCL );
            xtimer_usleep( m_togglePeriod_us );
        }

        if( gpio_read( m_pinSDA ) == 1 )
        {
            // Line successfully released
            break;
        }

        count++;
    }

    // Perform final health check
    ret = CheckForLatching();
    if( ret )
    {
        // Failed health check on SCL. Toggle will not fix this, so we abort.
        return ret;
    }
    else
    {
        return I2C_RETCODE_SUCCESS;
    }
}

i2c_retcode_t I2C::CheckBusHealth()
{
    SERCOM_I2CM_STATUS_Type status;
    auto ret = i2c_read_status_samd( m_i2cDev, &status );
    if( ret )
    {
        return ret;
    }

    // IMPROVE: Can make this fancier based on more things we can learn about the bus. Requires testing, since documentation can be vague.

    // Ultimately, if the bus state isn't idle, something is wrong (since this assumes a single threaded program).
    if( status.bit.BUSSTATE != I2C_BUS_STATE_IDLE )
    {
        // Invalid bus state
        return I2C_RETCODE_ERROR_INVALID_BUS_STATE;
    }

    // Bus is healthy
    return I2C_RETCODE_SUCCESS;
}

// -----------------------
// Transaction Methods
// -----------------------

// Write operations
i2c_retcode_t I2C::WriteByte( uint8_t slaveAddressIn, uint8_t dataIn )
{
    return HANDLE_RESULT( i2c_write_byte( m_i2cDev, slaveAddressIn, dataIn ) );
}

i2c_retcode_t I2C::WriteRegisterByte( uint8_t slaveAddressIn, uint8_t registerIn, uint8_t dataIn )
{
    return HANDLE_RESULT( i2c_write_reg( m_i2cDev, slaveAddressIn, registerIn, dataIn ) );
}

i2c_retcode_t I2C::WriteBytes( uint8_t slaveAddressIn, uint8_t *dataIn, uint8_t numberBytesIn )
{
    return HANDLE_RESULT( i2c_write_bytes( m_i2cDev, slaveAddressIn, (void*)dataIn, numberBytesIn ) );
}

i2c_retcode_t I2C::WriteRegisterBytes( uint8_t slaveAddressIn, uint8_t registerIn, uint8_t *dataIn, uint8_t numberBytesIn )
{
	return HANDLE_RESULT( i2c_write_regs( m_i2cDev, slaveAddressIn, registerIn, (void*)dataIn, numberBytesIn ) );
}

// Read operations
i2c_retcode_t I2C::ReadByte( uint8_t slaveAddressIn, uint8_t *dataOut )
{
    return HANDLE_RESULT( i2c_read_byte( m_i2cDev, slaveAddressIn, (void*)dataOut ) );
}

i2c_retcode_t I2C::ReadBytes( uint8_t slaveAddressIn, uint8_t *dataOut, uint8_t numberBytesIn )
{
    return HANDLE_RESULT( i2c_read_bytes( m_i2cDev, slaveAddressIn, (void*)dataOut, numberBytesIn ) );
}

i2c_retcode_t I2C::ReadRegisterByte( uint8_t slaveAddressIn, uint8_t registerIn, uint8_t *dataOut )
{
    return HANDLE_RESULT( i2c_read_reg( m_i2cDev, slaveAddressIn, registerIn, (void*)dataOut ) );
}

i2c_retcode_t I2C::ReadRegisterBytes( uint8_t slaveAddressIn, uint8_t registerIn, uint8_t *dataOut, uint8_t numberBytesIn )
{
    return HANDLE_RESULT( i2c_read_regs( m_i2cDev, slaveAddressIn, registerIn, (void*)dataOut, numberBytesIn ) );
}