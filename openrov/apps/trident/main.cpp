#include <xtimer.h>
#include <board.h>

#include "SystemMonitor.h"
#include "CommLink.h"
#include "I2CMonitor.h"

#include "MotorBlock.h"
#include "ForwardLights.h"
#include "StatusLights.h"

#include "DepthGauge.h"
#include "Barometer.h"
#include "FuelGauge.h"
#include "IMU.h"

namespace
{
    // Supervisors
    SystemMonitor   sysMonitor;                                 // Instantiates MCU system supervises all systems and subsystems
    CommLink        commLink;                                   // Instantiates and monitors the Mavlink channels
    I2CMonitor      i2cMonitor;                                 // Instantiates and monitors the I2C buses

    // Actuators
    MotorBlock      motorBlock;                                 // Aggregate of three systems and subsystems
    ForwardLights   forwardLights( PWM_0, 3, 11428, 4200 );     // Forward facing lights
    StatusLights    statusLights;                               // Tailboard lights
    
    // Sensors
    DepthGauge      depthGauge;                                 // Vertical motor depth sensor. Not on ASV
    Barometer       barometer;                                  // Pressure sensor
    FuelGauge       fuelGauge;                                  // Battery monitor
    IMU             imu;                                        // BNO055 and BNO080/BNO085
}


/**
 */


void Initialize()
{
    LOG_INFO( "\n----- INITIALIZE -----\n" );

    // Start by initializing the system monitor
    sysMonitor.Initialize();

    // Next, initialize the commlink. If this fails for the main channel, we treat it as fatal and reset.
    commLink.Initialize();

    // Initialize the I2C Monitor
    i2cMonitor.Initialize();

    // Initialize all of the devices
    motorBlock.Initialize();
    forwardLights.Initialize();
    statusLights.Initialize();
    depthGauge.Initialize();
    barometer.Initialize();
    fuelGauge.Initialize();
    imu.Initialize();
}

void Post()
{
    LOG_INFO( "\n----- POST -----\n" );

    // Handle the Power-on-self-test stage for all subsystems
    commLink.Post();                  // MAVLink master handler
    i2cMonitor.Post();                // I2C master handler

    // Post all of the devices
    motorBlock.Post();                // ESC Controller
    forwardLights.Post();             // PWM
    statusLights.Post();              // I2C
    depthGauge.Post();                // I2C
    barometer.Post();                 // I2C
    fuelGauge.Post();                 // I2C
    imu.Post();                       // I2C

    // Update system status
    sysMonitor.Update();
}

void Run()
{
    LOG_INFO( "\n----- RUN -----\n" );

    while( true )
    {
        // TODO: 
        // First part of the list would be get updates from devices which are known to be there
        // (in order of priority)
        
        
        // ----------------------------------
        // High priority updates

        // Receive messages from host
        commLink.Update();

        // Control-related updates
        motorBlock.Update();

        imu.Update();

        depthGauge.Update();

        // ----------------------------------
        // Mid Priority Updates

        // Update sensors
        barometer.Update();
        fuelGauge.Update();

        // Update actuators
        forwardLights.Update();
        statusLights.Update();

        // Update I2C Monitor
        i2cMonitor.Update();

        // Update system status
        sysMonitor.Update();
        
        if (I2CMonitor::doHardReset) {
            I2CMonitor::doHardReset = false;
            i2cMonitor.HardReset();

            I2CMonitor::re_init = true;

            Initialize();
            Post();
        }
    }
}

int main()
{
    Initialize();
    Post();

    // Infinite loop
    Run();

    return 0;
}
