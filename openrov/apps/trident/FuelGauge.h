#pragma once

// Includes
#include "I2CMonitor.h"
#include "Subsystem.h"

class FuelGauge : I2CDevice
{
public:
	enum class ESubstate : uint8_t
    {
        ACTIVE = static_cast<uint8_t>( EReservedSubstate::COUNT )
    };

	enum class ERetcode : uint8_t
	{
		SUCCESS = 0,
		ERROR_IO,
		ERROR_BAD_STATUS
	};

	// Methods
	explicit FuelGauge();

	void Initialize();
	void Post();
	void Update();

private:
	// Status parameters
	uint16_t m_voltage_mv;
	int16_t  m_avgCurrent_ma;
	int16_t  m_insCurrent_ma;
	uint32_t m_avgPower_mw;
	uint8_t  m_stateOfCharge_pct;
	uint16_t m_remainingCapacity_mah;
	uint32_t m_batteryTemperature_k;
	uint16_t m_flags;

	// Health parameters
	uint16_t m_fullCapacity_mah;
	uint16_t m_averageTimeToEmpty_mins;
	uint16_t m_cycleCount;
	uint8_t  m_stateOfHealth_pct;

    mavlink_orov_fuelgauge_status_t m_status;
    mavlink_orov_fuelgauge_health_t m_health;

	orutil::CTimer m_healthTimer;

	uint8_t m_postAttempts = 0;

	// Sensor methods
	ERetcode ReadStatus();
	ERetcode ReadHealth();

	void ReportStatus();
	void ReportHealth();

	ERetcode CheckHealth();

	// State methods
	bool PostAttempt();

	void Update_Active();
	void Update_Standby();
	void Update_Recovery();
	void Update_Disabled();
};
