#pragma once

#include <orutil.h>
#include <assert.h>
#include <log.h>
#include <i2cplus.h>

#include "Subsystem.h"
#include "CommLink.h"


enum class EI2CDeviceId : uint8_t
{
    BNO055 = 0,
    BNO080,
    MPL3115,
    MS5837,
    PCA9538,
    BQ34Z100G1,

    COUNT
};

enum class EI2CEvent : uint8_t
{
    POWER_RESET = 0,  // To signal devices that they need to reset their current state because devices have been power cycled
    BUS_AVAILABLE,    // To signal devices that the i2c bus has been brought back up and should be usable again
    BUS_UNAVAILABLE   // To signal devices that the i2c bus has been lost and they shouldn't bother trying to use it for now  
};

class I2CDevice
{
public:
    I2CDevice( EI2CDeviceId idIn, OROV_TRIDENT_SUBSYSTEM_ID systemId, 
               const char* nameIn, uint32_t failureLimitIn );
    
    void I2CEventCallback( EI2CEvent eventIn);
    
    void Callback( EI2CEvent eventIn )
    {
        if( m_callbackEnabled )
        {
            I2CEventCallback(eventIn);
        }
    }

    void EnableCallback()
    {
        m_callbackEnabled = true;
    }

    void DisableCallback()
    {
        m_callbackEnabled = false;
    }

    virtual void UpdateFailures( bool failureOccurred )
    {
        if( failureOccurred )
        {
            ++m_consecutiveFailures;
            mavlink_msg_debug_send(CommLink::MAIN_CHANNEL, 555, 0, m_consecutiveFailures);
            mavlink_msg_debug_send(CommLink::MAIN_CHANNEL, 556, 0, m_failureLimit);
        }
        else
        {
            m_consecutiveFailures = 0;
        }
    }

    void ResetFailures()
    {
        m_consecutiveFailures = 0;
    }

    uint32_t GetFailureCount()
    {
        return m_consecutiveFailures;
    }

    EI2CDeviceId GetId()
    {
        return m_id;
    }

    bool FailureLimitReached()
    {
        return ( m_consecutiveFailures >= m_failureLimit );
    }

    const char* GetName()
    {
        return m_name;
    }

    // String helpers
    static const char* EventToString( EI2CEvent eventIn )
    {
        switch( eventIn )
        {
            case EI2CEvent::POWER_RESET:     { return "POWER_RESET"; }
            case EI2CEvent::BUS_AVAILABLE:   { return "BUS_AVAILABLE"; }
            case EI2CEvent::BUS_UNAVAILABLE: { return "BUS_UNAVAILABLE"; }
            default: { return "UNKNOWN_BUS_STATE"; }
        }
    }
    
//protected:
    Subsystem m_subsystem;
    I2C*              m_i2c;

private:
    const EI2CDeviceId      m_id;
    const char*             m_name;

    const uint32_t          m_failureLimit;

    uint32_t                m_consecutiveFailures   = 0;
    bool                    m_callbackEnabled       = true;
};
