#pragma once
/*
 *  Notes:
 *  	Examples taken from SparkFun Arduino Example
 *  		https://github.com/sparkfun/SparkFun_BNO080_Arduino_Library
 */
#include <i2cplus.h>
#include <orutil.h>
#include <log.h>
#include <inttypes.h>
#include <cstddef>

#include <bno080_quaternion.h>
#include <bno080_definitions.h>
#include <bno.h>

namespace bno080 {
	class BNO080 : public BNO {
	public:
		BNO080(I2C* _dev, uint8_t _slaveAddr);
		virtual ~BNO080();

		i2c_retcode_t 			Reset( void );
        i2c_retcode_t           Configure( void );

		i2c_retcode_t 			VerifyChipId( bool &validChip );

		i2c_retcode_t           VerifyChipIdGet( uint8_t *data );

		i2c_retcode_t 			EnableAccel( void );
		i2c_retcode_t			EnableRotationVector( void );

		i2c_retcode_t 			EnableGyro( void );
		i2c_retcode_t			EnableMagnetometer( void );

		i2c_retcode_t			Calibrate( void );

		i2c_retcode_t 			ReadQuaternion( BNOQuaternion &quatOut );
        
        i2c_retcode_t           ReadGyro( TRawIMUData &dataOut );

		bool					DataAvailable( void );
		bool					ParseCommandReport();

		void					SetNewQuatMsgAvailable( bool newQuatMsg );
		void					SetNewAngVelMsgAvailable( bool newAngVelMsg );

		bool					GetNewQuatMsgAvailable();
		bool					GetNewAngVelMsgAvailable();

		float 					GetQuatRadianAccuracy();
		uint8_t 				GetQuatAccuracy();

		float 					GetAccelX();
		float 					GetAccelY();

		float 					GetAccelZ();
		uint8_t 				GetAccelAccuracy();

		float 					GetGyroX();
		float 					GetGyroY();

		float 					GetGyroZ();
		uint8_t 				GetGyroAccuracy();

		float 					GetMagX();
		float 					GetMagY();

		float 					GetMagZ();
		uint8_t 				GetMagAccuracy();

		float 					GetQuatI();
		float 					GetQuatJ();

		float 					GetQuatK();
		float 					GetQuatReal();

		float					QToFloat( int16_t fixedPointvalue, uint8_t qPoint );

		//These Q values are defined in the datasheet but can also be obtained by querying the meta data records
		//See the read metadata example for more info
		int16_t 				m_rotationVector_Q1 			= 14;
		int16_t					m_accelerometer_Q1 				= 8;

		int16_t 				m_gyro_Q1 						= 9;
		int16_t 				m_magnetometer_Q1 				= 4;

		uint32_t 				m_timeStamp;

        
        Quaternion				m_quat_imu_enu;		//ENU-->IMU rotation (Rotation Vector fused output from IMU)
		Quaternion				m_quat_imu_nwu;
		Quaternion				m_quat_z_180rot;	//NWU-->180 NWU rotation around z axis

		Quaternion 				m_quat_body_imu;	//IMU-->Body rotation
		Quaternion				m_quat_body_enu;	//ENU-->Body rotation
		Quaternion				m_quat_body_nwu;	//NWU-->Body rotation

		Vector<3>				m_ang_vel_imu;		//Angular velocity as measured by the IMU in rad/s
		Vector<3>				m_ang_vel_body;		//Angular velocity of vehicle in vehicle frame

		Vector<3>				m_rollPitchYaw080;
        
	private:
		i2c_retcode_t			SendPacket( uint8_t channel, uint8_t packetLength );
		i2c_retcode_t			SendCalPacket( uint8_t channel, uint8_t packetLength );

		void 					SetFeatureCommand( uint8_t reportID, uint16_t timeBetweenReports, uint32_t specificConfig );
		i2c_retcode_t 			ReadPackets( void );

		bool 					ParseData();

		bool					m_newQuat;
		bool					m_newAngVel;

		// These are the raw sensor values pulled from the user requested Input Report
		uint16_t 				m_rawAccelX, m_rawAccelY, m_rawAccelZ, m_accelAccuracy;
		uint16_t 				m_rawGyroX, m_rawGyroY, m_rawGyroZ, m_gyroAccuracy;
		uint16_t 				m_rawMagX, m_rawMagY, m_rawMagZ, m_magAccuracy;

		uint16_t 				m_rawQuatI, m_rawQuatJ, m_rawQuatK, m_rawQuatReal, m_rawQuatRadianAccuracy, m_quatAccuracy;
		uint16_t 				m_stepCount;
		uint8_t 				m_calibrationStatus;//Byte R0 of ME Calibration Response

		uint8_t 				shtpHeader[bno080::constants::HEADER_LEN];
		//uint8_t					shtpThrowAway[4];

		uint8_t 				shtpData[bno080::constants::MAX_PACKET_SIZE];
		uint8_t 				shtpPacket[bno080::constants::MAX_PACKET_SIZE];

		uint8_t 				sequenceNumber[6] = {0, 0, 0, 0, 0, 0}; //There are 6 com channels. Each channel has its own seqnum
		uint32_t				metaData[bno080::constants::MAX_METADATA_SIZE]; //There are more than 10 words in a metadata record but we'll stop at Q point 3
	};
} /* namespace bno080 */
