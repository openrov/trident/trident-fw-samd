
#include <bno055.h>

namespace bno055 {
    
    BNO055::BNO055(I2C* _dev, uint8_t _slaveAddr) : BNO(_dev, _slaveAddr)
    {        
    }
    
    // Methods
    i2c_retcode_t BNO055::Reset( void )
    {
        return dev->WriteRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::SYS_TRIGGER ), 0x20 );
    }

	i2c_retcode_t BNO055::VerifyChipId( bool &validChip )
    {
		uint8_t chipId = 0;
		auto ret = dev->ReadRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::CHIP_ID ), &chipId );
		validChip = ( chipId == constants::CHIP_ID );
        return ret;
    }
    
    i2c_retcode_t BNO055::EnableAccel( void )
    {
        return I2C_RETCODE_SUCCESS;
    }
    
    i2c_retcode_t BNO055::EnableRotationVector( void )
    {
        return I2C_RETCODE_SUCCESS;         
    }
    
    i2c_retcode_t BNO055::EnableGyro( void )
    {
        return I2C_RETCODE_SUCCESS;                                 
    }
    
    
    i2c_retcode_t BNO055::EnableMagnetometer( void )
    {
        return I2C_RETCODE_SUCCESS;                                 
    }
    
    i2c_retcode_t BNO055::Calibrate( void )
    {
        return I2C_RETCODE_SUCCESS;                                 
    }
    
    bool BNO055::DataAvailable( void ) {
        return true;
    }
    

    i2c_retcode_t BNO055::VerifyChipPOST( bool &successfulPost )
    {
		uint8_t post = 0;
		auto ret = dev->ReadRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::SELFTEST_RESULT ), &post );

		// Bit 0 = Accelerometer self test
        // Bit 1 = Magnetometer self test
        // Bit 2 = Gyroscope self test
        // Bit 3 = MCU self test
		successfulPost = ( ( post & 0x0F ) == 0x0F );

        return ret;
    }

    i2c_retcode_t BNO055::ReadSystemStatus( ESystemStatus &statusOut )
    {
		uint8_t status = 0;
		auto ret = dev->ReadRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::SYS_STATUS ), &status );
		statusOut = static_cast<ESystemStatus>( status );
		return ret;
    }

	i2c_retcode_t BNO055::ReadSystemError( uint8_t &errorOut )
    {
		return dev->ReadRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::SYS_ERR ), &errorOut );
    }

	i2c_retcode_t BNO055::ReadCalibration( TCalibrationData &calDataOut )
    {
		uint8_t calData = 0;
		auto ret = dev->ReadRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::CALIB_STAT ), &calData );
		if( ret ){ return ret; }
		
		calDataOut.mag 		= ( calData & 0x03 );
		calDataOut.accel 	= ( ( calData >> 2 ) & 0x03 );
		calDataOut.gyro 	= ( ( calData >> 4 ) & 0x03 );
		calDataOut.sys		= ( ( calData >> 6 ) & 0x03 );

		return ret;
    }

    i2c_retcode_t BNO055::ReadOperatingMode( EOpMode &opModeOut )
    {
		uint8_t opMode = 0;
		auto ret = dev->ReadRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::OPR_MODE ), &opMode );
		if( ret ){ return ret; }
		
		opModeOut = static_cast<EOpMode>( opMode & (0x0F) );

		return ret;
    }

	i2c_retcode_t BNO055::ReadQuaternion( BNOQuaternion &quatOut )
    {
		auto ret = dev->ReadRegisterBytes( slaveAddr, static_cast<uint8_t>( ERegister::QUATERNION_DATA_W_LSB ), g_tempBuffer, 8 );
		if( ret ){ return ret; }
		
		quatOut.w() = static_cast<float>( static_cast<int16_t>( ( static_cast<int16_t>( g_tempBuffer[1] ) << 8 ) | g_tempBuffer[0] ) ) / 16384.0f;
		quatOut.x() = static_cast<float>( static_cast<int16_t>( ( static_cast<int16_t>( g_tempBuffer[3] ) << 8 ) | g_tempBuffer[2] ) ) / 16384.0f;  
		quatOut.y() = static_cast<float>( static_cast<int16_t>( ( static_cast<int16_t>( g_tempBuffer[5] ) << 8 ) | g_tempBuffer[4] ) ) / 16384.0f;
		quatOut.z() = static_cast<float>( static_cast<int16_t>( ( static_cast<int16_t>( g_tempBuffer[7] ) << 8 ) | g_tempBuffer[6] ) ) / 16384.0f;

		return ret;
    }

	i2c_retcode_t BNO055::ReadGyro( TRawIMUData &dataOut )
    {
		auto ret = dev->ReadRegisterBytes( slaveAddr, static_cast<uint8_t>( ERegister::GYRO_DATA_X_LSB ), g_tempBuffer, 6 );
		if( ret ){ return ret; }
	
		dataOut.x = static_cast<float>( static_cast<int16_t>( ( static_cast<int16_t>( g_tempBuffer[1] ) << 8 ) | g_tempBuffer[0] ) ) / 900.0f;
		dataOut.y = static_cast<float>( static_cast<int16_t>( ( static_cast<int16_t>( g_tempBuffer[3] ) << 8 ) | g_tempBuffer[2] ) ) / 900.0f;
		dataOut.z = static_cast<float>( static_cast<int16_t>( ( static_cast<int16_t>( g_tempBuffer[5] ) << 8 ) | g_tempBuffer[4] ) ) / 900.0f;

		return ret;
    }

	i2c_retcode_t BNO055::SelectPage( const EPage& pageIn )
    {
		auto ret = dev->WriteRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::PAGE_ID ), static_cast<uint8_t>( pageIn ) );
		if( ret ){ return ret; }

		return ret;
    }

	i2c_retcode_t BNO055::SetPowerMode( const EPowerMode& modeIn )
    {
		auto ret = dev->WriteRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::PWR_MODE ), static_cast<uint8_t>( modeIn ) );
		if( ret ){ return ret; }

		return ret;
    }

	i2c_retcode_t BNO055::SetOperatingMode( const EOpMode& modeIn )
    {
		auto ret = dev->WriteRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::OPR_MODE ), static_cast<uint8_t>( modeIn ) );
		if( ret ){ return ret; }

		return ret;
    }

	i2c_retcode_t BNO055::SetAxisConfiguration( uint8_t axisConfig, uint8_t signConfig )
    {
		// AXIS_MAP_CONFIG:
		// Bits:	| 7		| 6 	| 5		| 4		| 3		| 2		| 1		| 0		|
		// 			| Reserved		| Remapped Z	| Remapped Y	| Remapped X	|
		// Bit Values: 
		// 00: X-axis
		// 01: Y-axis
		// 10: Z-axis
		// 11: Invalid

		auto ret = dev->WriteRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::AXIS_MAP_CONFIG ), axisConfig );
		if( ret ){ return ret; }

		// AXIS_MAP_SIGN:
		// Bits:	| 7		| 6 	| 5		| 4		| 3		| 2		| 1		| 0		|
		// 			| Reserved								| X 	| Y		| Z		|
		// Bit Values:
		// 0: Positive
		// 1: Negative

		ret = dev->WriteRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::AXIS_MAP_SIGN ), signConfig );
		if( ret ){ return ret; }

		return ret;
    }

	i2c_retcode_t BNO055::SetUnitConfiguration( uint8_t configIn )
    {
		auto ret = dev->WriteRegisterByte( slaveAddr, static_cast<uint8_t>( ERegister::UNIT_SEL ), configIn );
		if( ret ){ return ret; }

		return ret;
    }
    
    
    
    i2c_retcode_t BNO055::Configure()
    {
        // Configure the axes
        auto ret = SetAxisConfiguration(m_axisMap, m_axisSign );
        if( ret )
        {
            return ret;
        }

        // Configure the units
        ret = SetUnitConfiguration( m_unitSelect );
        if( ret )
        {
            return ret;
        }

        // Move to fusion mode
        ret = SetOperatingMode( bno055::EOpMode::NDOF );
        
        return ret;        
    }
        
    
    void BNO055::SetNewQuatMsgAvailable( bool newQuatMsg ) { 
        (void)newQuatMsg;
    } 
    
	void BNO055::SetNewAngVelMsgAvailable( bool newAngVelMsg ) {
        (void)newAngVelMsg;
    }    
        
    bool BNO055::GetNewQuatMsgAvailable() {
        return true;
    }
    
    bool BNO055::GetNewAngVelMsgAvailable() {
        return true;
    }
    
    float BNO055::GetGyroX() { return 0; }
    float BNO055::GetGyroY() { return 0; }
    float BNO055::GetGyroZ() { return 0; }
    
    float BNO055::GetQuatI() { return 0; }
    float BNO055::GetQuatJ() { return 0; }
    float BNO055::GetQuatK() { return 0; }
    float BNO055::GetQuatReal() { return 0; }
}



