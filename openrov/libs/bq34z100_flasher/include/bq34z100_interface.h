#pragma once

#include <i2cplus.h>

// Firmware file
#include "firmware/000001-02-fuel-gauge-firmware.h"

namespace bq34z100_interface
{
    // I2C Slave addresses, bit shifted left by one as per TI Datasheet
    namespace shifted_register_address
    {
        constexpr uint8_t DEVICE_ADDRESS = 0x55;
        constexpr uint8_t ROM_MODE_ADDRESS = 0x0b;
    }

    // I2C Commands
    namespace commands
    {
        constexpr uint8_t CONTROL = 0x00;
        constexpr uint8_t STATE_OF_CHARGE = 0x02;
        constexpr uint8_t MAX_ERROR = 0x03;
        constexpr uint8_t REMAINING_CAPACITY = 0x04;
        constexpr uint8_t FULL_CHARGE_CAPACITY = 0x06;
        constexpr uint8_t VOLTAGE = 0x08;
        constexpr uint8_t AVERAGE_CURRENT = 0x0A;
        constexpr uint8_t TEMPERATURE = 0x0C;
        constexpr uint8_t FLAGS = 0x0E;
        constexpr uint8_t CURRENT = 0x10;
        constexpr uint8_t FLAGSB = 0x12;
    }

    namespace extended_commands
    {
        constexpr uint8_t AVERAGE_TIME_TO_EMPTY = 0x18;
        constexpr uint8_t AVERAGE_TIME_TO_FULL = 0x1A;
        constexpr uint8_t PASSED_CHARGE = 0x1C;
        constexpr uint8_t DOD0_TIME = 0x1E;
        constexpr uint8_t AVAILABLE_ENERGY = 0x24;
        constexpr uint8_t AVERAGE_POWER = 0x26;
        constexpr uint8_t SERIAL_NUMBER = 0x28;
        constexpr uint8_t INTERNAL_TEMPERATURE = 0x2A;
        constexpr uint8_t CYCLE_COUNT = 0x2C;
        constexpr uint8_t STATE_OF_HEALTH = 0x2E;
        constexpr uint8_t CHARGE_VOLTAGE = 0x30;
        constexpr uint8_t CHARGE_CURRENT = 0x32;
        constexpr uint8_t DESIGN_CAPACITY = 0x3A;
        constexpr uint8_t LEARNED_STATUS = 0x63;
    }

    // I2C Sub commands
    namespace control_subcommands
    {
        constexpr uint8_t CONTROL_STATUS_LSB = 0x00;
        constexpr uint8_t CONTROL_STATUS_MSB = 0x00;

        constexpr uint8_t FW_VERSION_LSB = 0x02;
        constexpr uint8_t FW_VERSION_MSB = 0x00;
        
        constexpr uint8_t SEALED_LSB = 0x20;
        constexpr uint8_t SEALED_MSB = 0x00;
        
        constexpr uint8_t IT_ENABLE_LSB = 0x21;
        constexpr uint8_t IT_ENABLE_MSB = 0x00;

        constexpr uint8_t RESET_LSB = 0x41;
        constexpr uint8_t RESET_MSB = 0x00;
    }

    namespace firmware_keycodes
    {
        constexpr uint8_t NEW_LINE = 0x0a;
        constexpr uint8_t COMMENT = 0x3b;
        constexpr uint8_t COMPARE = 0x43;
        constexpr uint8_t UNSEALED_MARK = 0x55;
        constexpr uint8_t WAIT = 0x58;
        constexpr uint8_t WRITE = 0x57;  
    }

    // Length of binary data for the battery firmware
    static constexpr size_t BATTERY_FIRMWARE_LENGTH = (sizeof(k_battery)/sizeof(*k_battery));

    // "Each line of the file represents one command and potentially 96 bytes of data..."
    // Reference: http://www.ti.com/lit/an/slua541a/slua541a.pdf
    constexpr int MAX_DATA_BYTES_PER_LINE = 1024;

    // Methods
    i2c_retcode_t ITEnable(I2C* dev);
    i2c_retcode_t Reset(I2C* dev, uint8_t slaveAddr);
    i2c_retcode_t Seal(I2C* dev);

    // Reads two bytes after issuing command
    inline i2c_retcode_t ReadCommandBytes(I2C* dev, const uint8_t& commandIn, uint8_t* dataOut)
    {
        return dev->ReadRegisterBytes(shifted_register_address::DEVICE_ADDRESS, commandIn, dataOut, 2 );
    }
    // Reads two bytes after issuing control command and subcommand
    inline i2c_retcode_t ReadControlSubcommandBytes( I2C* dev, const uint8_t& subcommandIn, uint8_t* dataOut )
    {
        // Issue a control command with the specified subcommand
        uint8_t subcommand[2] = { subcommandIn, 0x00 };
        auto ret = dev->WriteRegisterBytes(shifted_register_address::DEVICE_ADDRESS, commands::CONTROL, subcommand, 2 );
        if( ret )
        {
            return ret;
        }

        // Read the result of the subcommand
        return dev->ReadRegisterBytes(shifted_register_address::DEVICE_ADDRESS, commands::CONTROL, dataOut, 2 );
    }
    // Status methods
    i2c_retcode_t IsITEnabled(I2C* dev, bool &isITEnabledOut);
    i2c_retcode_t IsSealed(I2C* dev, bool &isSealedOut);

    i2c_retcode_t ReadAverageCurrent(I2C* dev, int16_t &currentOut_ma);
    i2c_retcode_t ReadAveragePower(I2C* dev, uint32_t &powerOut_mw);
    i2c_retcode_t ReadInstantCurrent(I2C* dev, int16_t &currentOut_ma);
    i2c_retcode_t ReadRemainingCapacity(I2C* dev, uint16_t &capacityOut_mah);
    i2c_retcode_t ReadTemperature(I2C* dev, uint32_t &temperatureOut_k);
    i2c_retcode_t ReadFlags(I2C* dev, uint16_t &flagsOut);
    i2c_retcode_t ReadStateOfCharge(I2C* dev, uint8_t &percentOut);
    i2c_retcode_t ReadFirmwareVersion(I2C* dev, uint8_t* firmwareVersionOut);
    i2c_retcode_t ReadFirmwareVersionROM(I2C* dev, uint8_t* firmwareVersionOut);
    i2c_retcode_t ReadVoltage(I2C* dev, uint16_t& voltageMVOut);
}