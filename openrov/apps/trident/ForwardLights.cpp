// Includes
#include "ForwardLights.h"
#include "CommLink.h"
#include "SystemMonitor.h"

ForwardLights::ForwardLights( pwm_t pwmDevIn, uint8_t pwmChannelIn, uint32_t pwmFrequencyIn, uint16_t pwmResolutionIn )
	: m_subsystem{ OROV_TRIDENT_SUBSYSTEM_ID_FORWARD_LIGHTS, "FLIG" }
    , m_pwmDev( pwmDevIn )
    , m_pwmChannel( pwmChannelIn )
    , m_pwmFrequency( pwmFrequencyIn )
    , m_pwmResolution( pwmResolutionIn )
{
}

void ForwardLights::Initialize()
{
    // Set status pointer
    SystemMonitor::RegisterSubsystem( &m_subsystem );

	// Reset timers
    m_controlTimer.Reset();
    
    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_INITIALIZED );
}

void ForwardLights::Post()
{
    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_POSTING );
    m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );

    // Initialize the PWM interface
	if( !pwm_init( m_pwmDev, pwm_mode_t::PWM_LEFT, m_pwmFrequency, m_pwmResolution ) )
    {
        // Post failed. Unrecoverable.
        m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_DISABLED );
        m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

        LOG_DEBUG( "ForwardLights::Post: Failure!\n" );
    }
    else
    {
        m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
        m_subsystem.SetPostResult( OROV_POST_RESULT_SUCCESS );

        // Initiate a greeting as the first action
        m_subsystem.TransitionSubstate( FORWARD_LIGHTS_SUBSTATE_GREETING );
        
        LOG_DEBUG( "ForwardLights::Post: Success!\n" );
    }
}

void ForwardLights::HandleMessages()
{
	int messageId = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessageID();
    
    switch( messageId )
    {
		case MAVLINK_MSG_ID_OROV_SUBSYSTEM_DATA_REQUEST:
        {
            mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();
            uint8_t targetMsgId = mavlink_msg_orov_subsystem_data_request_get_msg_id( message );

            if( targetMsgId == MAVLINK_MSG_ID_OROV_FORWARD_LIGHT_STATE )
            {
                // Signify spontaneous response
                m_state.sequence = -1;

                mavlink_msg_orov_forward_light_state_send_struct( CommLink::MAIN_CHANNEL, &m_state );
            }

			break;
		}

        case MAVLINK_MSG_ID_OROV_FORWARD_LIGHT_STATE:
        {
            mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();

            // Update the sequence number
			m_state.sequence = mavlink_msg_orov_forward_light_state_get_sequence( message );

            // Update the target power
			m_state.target_power = mavlink_msg_orov_forward_light_state_get_target_power( message );

			// Convert percent to pwm duty cycle value
			m_targetPower_pwm = PercentToDutyCycle( m_state.target_power );

			// Apply ceiling
			if( m_targetPower_pwm > m_pwmResolution )
			{
				m_targetPower_pwm = m_pwmResolution;
			}

			// Directly move to target power
			m_state.current_power   = m_state.target_power;
			m_currentPower_pwm 	    = m_targetPower_pwm;

			// Write the power value to the pin
			pwm_set( m_pwmDev, m_pwmChannel, m_targetPower_pwm );

            // Emit updated status
            mavlink_msg_orov_forward_light_state_send_struct( CommLink::MAIN_CHANNEL, &m_state );
            
            break;
        }

        case MAVLINK_MSG_ID_OROV_SIMPLE_COMMAND:
        {
            mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();

            // Get command type
            auto command = mavlink_msg_orov_simple_command_get_command( message );
            
            if( command == OROV_CMD::OROV_CMD_GREETING )
            {
                // Get sequence number
                auto sequenceId = mavlink_msg_orov_simple_command_get_sequence( message );

                // Echo command back to sending system to ACK
                mavlink_msg_orov_simple_command_send( CommLink::MAIN_CHANNEL, sequenceId, command );

                // Move to greeting state substate and reset timer
                m_subsystem.TransitionSubstate( FORWARD_LIGHTS_SUBSTATE_GREETING );
                m_controlTimer.Reset();

                // Set light state to off and reset greet cycles
                m_greetState = 0;
                m_greetCycle = 0;

                pwm_set( m_pwmDev, m_pwmChannel, PercentToDutyCycle( 0.0f ) );
            }

            break;
        }

		default:
		{
			break;
		}
	}
}

void ForwardLights::Update()
{
    HandleMessages();

    switch( m_subsystem.GetState() )
    {
        case OROV_SUBSYSTEM_STATE_ACTIVE:
        {
            ActiveState();
            break;
        }

        default:
        {
            // Do nothing
            break;
        }
    }
}

void ForwardLights::ActiveState()
{
    // Only current action is doing greeting. Later, there will be more sophisticated ramping functions for dimming here.
    if( m_subsystem.GetSubstate() == FORWARD_LIGHTS_SUBSTATE_GREETING )
    {
        if( m_controlTimer.HasElapsed( m_greetDelay_us ) )
        {
           // Set to opposite state
		   if( m_greetState )
		   {
			   m_greetState = 0;
			   pwm_set( m_pwmDev, m_pwmChannel, PercentToDutyCycle( 1.0f ) );
		   }
		   else
		   {
			   m_greetState = 1;
			   pwm_set( m_pwmDev, m_pwmChannel, PercentToDutyCycle( 0.0f ) );
		   }

		   m_greetCycle++;

		   if( m_greetCycle >= m_cycleCount )
		   {
			   // Reset pin back to its original value
			   pwm_set( m_pwmDev, m_pwmChannel, m_targetPower_pwm );

               // Move back to idle state
               m_subsystem.TransitionSubstate( FORWARD_LIGHTS_SUBSTATE_IDLE );
		   }
        }
    }
}

uint16_t ForwardLights::PercentToDutyCycle( float percentIn )
{
    if( percentIn < 0.0f )
    {
        // Specifically floor negatives here, since the input is signed and output is unsigned
        return 0;
    }
    else
    {
        return static_cast<uint16_t>( percentIn * m_pwmResolution );
    }
}