#include "SystemMonitor.h"
#include "CommLink.h"

#include <panic.h>
#include <periph/gpio.h>
#include <xtimer.h>

// Initialize static members
MavlinkChannel CommLink::MavlinkChannels[ MAVLINK_COMM_NUM_BUFFERS ] = 
{ 
    { UART_DEV_MAIN,        kMainBaudRate,  MAIN_CHANNEL },
    { UART_DEV_PORTMOTOR,   kMotorBaudRate, PORT_CHANNEL },
    { UART_DEV_VERTMOTOR,   kMotorBaudRate, VERT_CHANNEL },
    { UART_DEV_STARMOTOR,   kMotorBaudRate, STAR_CHANNEL }
};

MavlinkChannel* CommLink::GetChannel( mavlink_channel_t channelIn )
{
    assert(( channelIn < MAVLINK_COMM_NUM_BUFFERS ));
    return &CommLink::MavlinkChannels[ channelIn ];
}

// Implement mavlink send function
void mavlink_send_uart_bytes( mavlink_channel_t channelIn, const uint8_t *dataIn, int lengthIn )
{
    assert(( channelIn < MAVLINK_COMM_NUM_BUFFERS ));

    // This puts the passed in data into the transmit buffer, if there is room
    CommLink::GetChannel( channelIn )->Write( dataIn, lengthIn );
}

/////////////////////////

void CommLink::Initialize()
{
    // Register as a subsystem
    SystemMonitor::RegisterSubsystem( &m_subsystem );

    // Initialize the main channel
    auto ret = MavlinkChannels[ MAIN_CHANNEL ].Initialize();

    // Failure to initialize the main comm channel is a critical failure. Restart the chip.
    if( ret > 0 )
    {
        core_panic( PANIC_GENERAL_ERROR, "Failed to initialize Main UART Channel!\n" );
    }

    MavlinkChannels[ PORT_CHANNEL ].Initialize();
    MavlinkChannels[ VERT_CHANNEL ].Initialize();
    MavlinkChannels[ STAR_CHANNEL ].Initialize();
    
    m_reportTimer.Reset();

    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_INITIALIZED );
}

bool CommLink::PostCheck()
{
    return MavlinkChannels[ PORT_CHANNEL ].IsAvailable() && 
           MavlinkChannels[ VERT_CHANNEL ].IsAvailable() && 
           MavlinkChannels[ STAR_CHANNEL ].IsAvailable();
}

void CommLink::Post()
{
    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_POSTING );
    m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );

    int postCount = 0;

    while( (PostCheck() == false) && ( postCount < 3 ) )
    {
        LOG_DEBUG( "CommLink::Post attempt\n" );

        MavlinkChannels[ PORT_CHANNEL ].Initialize();
        MavlinkChannels[ VERT_CHANNEL ].Initialize();
        MavlinkChannels[ STAR_CHANNEL ].Initialize();

        postCount++;
    }

    if( PostCheck() == true )
    {
        // Good. Update post status
        m_subsystem.SetPostResult( OROV_POST_RESULT_SUCCESS );
        LOG_DEBUG( "CommLink::Post: Success!\n" );
    }
    else
    {
        // Bad. Update post status
        m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );
        LOG_DEBUG( "CommLink::Post: Failure!\n" );
    }

    // TODO: Is there really anything we can do here in terms of recovery?
    m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
}

void CommLink::Update()
{
    // Receive messages from the CPU
    MavlinkChannels[ MAIN_CHANNEL ].Update();

    // Motor mavlink channels are updated by the motorblock subsystem, since they are responsible for all communication on them
    // MavlinkChannels[ PORT_CHANNEL ].Update();
    // MavlinkChannels[ VERT_CHANNEL ].Update();
    // MavlinkChannels[ STAR_CHANNEL ].Update();

    // if( m_reportTimer.HasElapsed( 1000000 ) )
    // {
    //     ReportLinkStats();
    // }
}

void CommLink::ReportLinkStats()
{
    LOG_DEBUG( "-----------\nCommLink Stats:\n" );
    LOG_DEBUG( "MAIN: TX:%" PRIu32 " RX:%" PRIu32 "\n", MavlinkChannels[ MAIN_CHANNEL ].GetTxByteCount(), MavlinkChannels[ MAIN_CHANNEL ].GetRxByteCount() );
    LOG_DEBUG( "PORT: TX:%" PRIu32 " RX:%" PRIu32 "\n", MavlinkChannels[ PORT_CHANNEL ].GetTxByteCount(), MavlinkChannels[ PORT_CHANNEL ].GetRxByteCount() );
    LOG_DEBUG( "VERT: TX:%" PRIu32 " RX:%" PRIu32 "\n", MavlinkChannels[ VERT_CHANNEL ].GetTxByteCount(), MavlinkChannels[ VERT_CHANNEL ].GetRxByteCount() );
    LOG_DEBUG( "STAR: TX:%" PRIu32 " RX:%" PRIu32 "\n", MavlinkChannels[ STAR_CHANNEL ].GetTxByteCount(), MavlinkChannels[ STAR_CHANNEL ].GetRxByteCount() );

    m_stats.mav_parse_fail_count = MavlinkChannels[ MAIN_CHANNEL ].GetBadCRCCount();
    m_stats.queue_overflow_count = MavlinkChannels[ MAIN_CHANNEL ].GetDroppedWhenFullCount();

    // Send stats
    mavlink_msg_orov_trident_comm_link_stats_send_struct( CommLink::MAIN_CHANNEL, &m_stats );
}




