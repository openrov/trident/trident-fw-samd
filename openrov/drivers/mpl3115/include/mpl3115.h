#pragma once

#include <i2cplus.h>
#include <orutil.h>
#include <log.h>

namespace mpl3115
{
    // I2C Slave Address
    // NOTE: This device only has a single address available
    namespace address
    {
        constexpr uint8_t A = 0x60;
    }

    // Oversample rates for ADC conversions
    enum class EOversampleRate : uint8_t
    {
        OSR_1_SAMPLES = 0,
        OSR_2_SAMPLES,
        OSR_4_SAMPLES,
        OSR_8_SAMPLES,
        OSR_16_SAMPLES,
        OSR_32_SAMPLES,
        OSR_64_SAMPLES,
        OSR_128_SAMPLES,

        _OSR_COUNT
    };

    // Device I2C Registers
    enum class ERegister : uint8_t
    {
        STATUS                      = 0x00,
        
        PRESSURE_OUT_MSB            = 0x01,
        PRESSURE_OUT_CSB            = 0x02,
        PRESSURE_OUT_LSB            = 0x03,

        TEMP_OUT_MSB                = 0x04,
        TEMP_OUT_LSB                = 0x05,

        DR_STATUS                   = 0x06,

        PRESSURE_OUT_DELTA_MSB      = 0x07,
        PRESSURE_OUT_DELTA_CSB      = 0x08,
        PRESSURE_OUT_DELTA_LSB      = 0x09,

        TEMP_OUT_DELTA_MSB          = 0x0A,
        TEMP_OUT_DELTA_LSB          = 0x0B,

        WHO_AM_I                    = 0x0C,

        FIFO_STATUS                 = 0x0D,
        FIFO_DATA                   = 0x0E,
        FIFO_SETUP                  = 0x0F,

        TIME_DELAY                  = 0x10,

        SYSMOD                      = 0x11,

        INT_SOURCE                  = 0x12,

        PT_DATA_CFG                 = 0x13,

        BAR_INPUT_MSB               = 0x14,
        BAR_INPUT_LSB               = 0x15,

        PRESSURE_TARGET_MSB         = 0x16,
        PRESSURE_TARGET_LSB         = 0x17,
        
        TEMP_TARGET                 = 0x18,

        PRESSURE_WINDOW_MSB         = 0x19,
        PRESSURE_WINDOW_LSB         = 0x1A,

        TEMP_WINDOW                 = 0x1B,

        MIN_PRESSURE_DATA_OUT_MSB   = 0x1C,
        MIN_PRESSURE_DATA_OUT_CSB   = 0x1D,
        MIN_PRESSURE_DATA_OUT_LSB   = 0x1E,

        MIN_TEMP_DATA_OUT_MSB       = 0x1F,
        MIN_TEMP_DATA_OUT_LSB       = 0x20,

        MAX_PRESSURE_DATA_OUT_MSB   = 0x21,
        MAX_PRESSURE_DATA_OUT_CSB   = 0x22,
        MAX_PRESSURE_DATA_OUT_LSB   = 0x23,

        MAX_TEMP_DATA_OUT_MSB       = 0x24,
        MAX_TEMP_DATA_OUT_LSB       = 0x25,

        CONTROL_REGISTER_1          = 0x26,
        CONTROL_REGISTER_2          = 0x27,
        CONTROL_REGISTER_3          = 0x28,
        CONTROL_REGISTER_4          = 0x29,
        CONTROL_REGISTER_5          = 0x2A,

        PRESSURE_DATA_OFFSET        = 0x2B,
        TEMP_DATA_OFFSET            = 0x2C,
        ALT_DATA_OFFSET             = 0x2D
    };

    namespace ctrlreg1_bits
    {
        constexpr uint8_t BAROMETER_MODE    = ( 0 << 7 );
        constexpr uint8_t RESET             = ( 1 << 2 );
        constexpr uint8_t SBYB_ACTIVE       = ( 1 << 0 );
    }

    // Computation constants
    namespace constant
    {
        constexpr uint8_t CHIP_ID = 0xC4;

        // Periods that the user code should use for timing purposes
        constexpr uint32_t DATA_READY_CHECK_DELAY_US    = 200;
	    constexpr uint32_t POR_DELAY_US                 = 500000;

        // Table of command modifiers and conversion times for each OSR
        constexpr uint32_t kOSRInfo[ (uint8_t)EOversampleRate::_OSR_COUNT ] =
        {
            // Delay(us)
            6000,   // 1
            10000,  // 2
            18000,  // 4
            34000,  // 8
            66000,  // 16
            130000, // 32
            258000, // 64
            512000  // 128
        };
    }
    
    // Globals used to efficiently perform conversion calculations
    // These should not be directly modified by user code
    extern uint8_t g_data[5];
    extern uint8_t g_status;
    extern uint8_t g_settings;
    extern uint8_t g_msb;
    extern uint8_t g_csb;
    extern uint8_t g_lsb;
    extern uint32_t g_totalPressure;

    // Methods
    inline i2c_retcode_t Reset( I2C* dev )
    {
        return dev->WriteRegisterByte( address::A, static_cast<uint8_t>(ERegister::CONTROL_REGISTER_1), ctrlreg1_bits::RESET );
    }

    inline i2c_retcode_t ReadChipId( I2C* dev, uint8_t &chipIdOut )
    {
        return dev->ReadRegisterByte( address::A, static_cast<uint8_t>(ERegister::WHO_AM_I), &chipIdOut );
    }

    inline i2c_retcode_t ConfigureBarometer( I2C* dev, EOversampleRate osrIn )
    {
        g_settings = 0;                               // Barometer mode
        g_settings |= static_cast<uint8_t>( osrIn ) << 3;     // Configure OSR
        g_settings |= mpl3115::ctrlreg1_bits::SBYB_ACTIVE;    // ACTIVE mode

        // Write new setting
        auto ret = dev->WriteRegisterByte( address::A, static_cast<uint8_t>(ERegister::CONTROL_REGISTER_1), g_settings );
        if( ret )
        {   // Failure
            return ret;
        }

        // Enable all data ready events
        ret = dev->WriteRegisterByte( address::A, static_cast<uint8_t>(ERegister::PT_DATA_CFG), 0x07 );
        if( ret )
        {   // Failure
            return ret;
        }

        return ret;
    }

    inline i2c_retcode_t ReadControlRegister1( I2C* dev, uint8_t &dataOut )
    {
        return dev->ReadRegisterByte( address::A, static_cast<uint8_t>(ERegister::CONTROL_REGISTER_1), &dataOut );
    }

    inline i2c_retcode_t StartMeasurement( I2C* dev )
    {
        g_settings = 0;

        // Read ctrl1 register to get current values
        auto ret = dev->ReadRegisterByte( address::A, static_cast<uint8_t>(ERegister::CONTROL_REGISTER_1), &g_settings );
        if( ret )
        {   // Failure
            return ret;
        }

        // Clear OST bit
        g_settings &= ~( 1 << 1 );
        ret = dev->WriteRegisterByte( address::A, static_cast<uint8_t>(ERegister::CONTROL_REGISTER_1), g_settings );
        if( ret )
        {   // Failure
            return ret;
        }

        // Read settings again
        ret = dev->ReadRegisterByte( address::A, static_cast<uint8_t>(ERegister::CONTROL_REGISTER_1), &g_settings );
        if( ret )
        {   // Failure
            return ret;
        }

        // Set OST bit to start new measurement
        g_settings |= ( 1 << 1 );
        ret = dev->WriteRegisterByte( address::A, static_cast<uint8_t>(ERegister::CONTROL_REGISTER_1), g_settings );
        if( ret )
        {   // Failure
            return ret;
        }

        // Success
        return ret;
    }

    inline i2c_retcode_t CheckDataReady( I2C* dev, bool &dataAvailable )
    {
        // Read Status register
        g_status = 0;
        auto ret = dev->ReadRegisterByte( address::A, static_cast<uint8_t>( ERegister::STATUS ), &g_status );
        if( ret )
        {   // Failure
            return ret;
        }

        // Check PDR bit
        if( ( g_status & ( 1 << 2 ) ) == 0 )
        {
            dataAvailable = false;
        }
        else
        {
            dataAvailable = true;
        }

        return ret;
    }

    // NOTE: Result provided by sensor in pascals. Divide by 100 to get millibars
    inline i2c_retcode_t ReadPressureAndTemp( I2C* dev, float &pressureOut, float &temperatureOut )
    {
        // Read data registers
        auto ret = dev->ReadRegisterBytes( address::A, static_cast<uint8_t>( ERegister::PRESSURE_OUT_MSB ), g_data, 5 );
        if( ret )
        {   // Failure
            return ret;
        }

        // Get pressure
        g_msb = g_data[0];
        g_csb = g_data[1];
        g_lsb = g_data[2];

        g_totalPressure = (((uint32_t)(g_msb)<<16) | (((uint32_t)g_csb)<<8) | (uint32_t)g_lsb );

        //Pressure is an 18 bit number with 2 bits of decimal. Get rid of decimal portion.
        g_totalPressure >>= 6;

        //Bits 5/4 represent the fractional component
        g_lsb &= 0x30; //B00110000
        
        //Get it right aligned
        g_lsb >>= 4;

        // Calculate final pressure
        pressureOut = ( (float)g_totalPressure + ((float)g_lsb/4.0f) ) / 100.0f;

        g_msb = g_data[3];
        g_lsb = g_data[4];

        // Get temperature, the 12-bit temperature measurement in °C is comprised of a signed integer component and a fractional component.
        // The signed 8-bit integer component is located in OUT_T_MSB. 
        // The fractional component is located in bits 7-4 of OUT_T_LSB.
        // Bits 3-0 of OUT_T_LSB are not used.

        temperatureOut = (float)((uint32_t)g_msb) + (float)(g_lsb >> 4) * 0.0625f;

        return ret;
    }
    
}