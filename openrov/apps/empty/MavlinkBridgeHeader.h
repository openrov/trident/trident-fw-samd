#pragma once

// Set Mavlink definitions here before including mavlink headers to configure
#define MAVLINK_USE_CONVENIENCE_FUNCTIONS
#define MAVLINK_SEND_UART_BYTES mavlink_send_uart_bytes

#define MAVLINK_COMM_NUM_BUFFERS 4

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <mavlink/v1.0/mavlink_types.h>
#pragma GCC diagnostic pop

#include <unistd.h>

extern mavlink_system_t mavlink_system;

void mavlink_send_uart_bytes( mavlink_channel_t channelIn, const uint8_t *dataIn, int lengthIn );

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <mavlink/v1.0/openrov/mavlink.h>
#pragma GCC diagnostic pop