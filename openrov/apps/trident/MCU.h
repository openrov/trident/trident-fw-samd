#pragma once

#include <cstring>
#include <assert.h>
#include <orutil.h>
#include <log.h>

#include "MavlinkBridgeHeader.h"

class MCU
{
public:
    // Default string values
    // TODO: Compile time length checks of such values
    static constexpr const char* kDefaultVersion    = "UNKNOWN";
    static constexpr const char* kDefaultAppId      = "UNKWN";
    static constexpr const char* kDefaultBoardId    = "UNKNOWN__";

    explicit MCU( OROV_TRIDENT_MCU_ID idIn )
        : k_id{ idIn }
    {
        // Initialize status information
        m_status.mcu_id 					= k_id;
        m_status.flash_crc_result		    = OROV_FLASH_CRC_RESULT_WAITING;
        m_status.reset_cause				= OROV_MCU_RESET_CAUSE_UNKNOWN;
        m_status.reset_extra				= 0;
        m_status.uptime_secs				= 0;
        m_status.mavlink_version   		    = 0;
        strncpy( m_status.version,  kDefaultVersion,   MAVLINK_MSG_OROV_MCU_STATUS_FIELD_VERSION_LEN );
        strncpy( m_status.app_id,   kDefaultAppId,     MAVLINK_MSG_OROV_MCU_STATUS_FIELD_APP_ID_LEN );
        strncpy( m_status.board_id, kDefaultBoardId,   MAVLINK_MSG_OROV_MCU_STATUS_FIELD_BOARD_ID_LEN );
    }

    // Pointer Access and Update
    mavlink_orov_mcu_status_t* GetStatus()          { return &m_status; }

    void UpdateStatus( mavlink_message_t* msgIn )   { mavlink_msg_orov_mcu_status_decode( msgIn, &m_status ); m_status.mcu_id = k_id; }

    // Status Accessors
    OROV_TRIDENT_MCU_ID GetId()                     { return static_cast<OROV_TRIDENT_MCU_ID>( k_id ); }
    OROV_FLASH_CRC_RESULT GetFlashCRCResult()       { return static_cast<OROV_FLASH_CRC_RESULT>( m_status.flash_crc_result ); }
    OROV_MCU_RESET_CAUSE_TYPE GetResetCause()       { return static_cast<OROV_MCU_RESET_CAUSE_TYPE>( m_status.reset_cause ); }
    uint8_t GetResetExtra()                         { return m_status.reset_extra; }
    uint32_t GetUptimeSecs()                        { return m_status.uptime_secs; }
    uint8_t GetMavlinkVersion()                     { return m_status.mavlink_version; }
    const char* GetVersion()                        { return m_status.version; }
    const char* GetAppId()                          { return m_status.app_id; }
    const char* GetBoardId()                        { return m_status.board_id; }

    void SetId( uint8_t idIn )                                  { m_status.mcu_id = idIn; }
    void SetFlashCRCResult( OROV_FLASH_CRC_RESULT resultIn )    { m_status.flash_crc_result = resultIn; }
    void SetResetCause( uint8_t causeIn )                       { m_status.reset_cause = causeIn; }
    void SetResetExtra( uint8_t extraIn )                       { m_status.reset_extra = extraIn; }
    void SetUptimeSecs( uint32_t secondsIn )                    { m_status.uptime_secs = secondsIn; }
    void SetMavlinkVersion( uint8_t versionIn )                 { m_status.mavlink_version = versionIn; }
    
    void SetVersion( const char* const versionIn )              
    { 
        // TODO: Issue some kind of alert here, though it should never happen in practice.
        orutil::safecopy( m_status.version, versionIn, MAVLINK_MSG_OROV_MCU_STATUS_FIELD_VERSION_LEN );
    }

    void SetAppId( const char* const appIdIn )              
    { 
        // TODO: Issue some kind of alert here, though it should never happen in practice.
        orutil::safecopy( m_status.app_id, appIdIn, MAVLINK_MSG_OROV_MCU_STATUS_FIELD_APP_ID_LEN );
    }

    void SetBoardId( const char* const boardIdIn )              
    { 
        // TODO: Issue some kind of alert here, though it should never happen in practice.
        orutil::safecopy( m_status.board_id, boardIdIn, MAVLINK_MSG_OROV_MCU_STATUS_FIELD_BOARD_ID_LEN );
    }

    // Used for pretty printing debug information
    void Debug_ReportStatus()
    {
        // TODO: Rework this
        // LOG_INFO( "Sys: %d | Board: %s | Hash: %s | Flash Test: %d | Uptime: %lu | LPS: %ld\n", 
        //     m_status.id, 
        //     m_version.board_id,
        //     m_version.git_hash,
        //     m_version.flash_crc_result,
        //     m_status.uptime,
        //     m_status.loops_per_sec );
    }

    // Mavlink Send Methods
    void SendStatus( mavlink_channel_t channelIn )
    {
        mavlink_msg_orov_mcu_status_send_struct( channelIn, &m_status );
    }

private:
    // Keep these private to ensure proper use via accessors
    mavlink_orov_mcu_status_t m_status;
    const uint8_t k_id;
};
