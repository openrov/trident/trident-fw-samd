#include "NState.h"


namespace state
{
    bool isShallow = true;

    float depth             = 0.0f;
    float water_temp        = 0.0f;
    float internal_pressure = 0.0f;
    float internal_temp     = 0.0f;
    float voltage           = 0.0f;
    float current           = 0.0f;
    float remainingPower    = 0.0f;
    uint8_t discharging        = 0;
}