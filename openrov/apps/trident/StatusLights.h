#pragma once

// Includes
#include <pca9538.h>
#include "I2CMonitor.h"
#include "Subsystem.h"

class StatusLights : I2CDevice
{
public:
	enum class ESubstate : uint8_t
    {
        POWER_PATTERN = static_cast<uint8_t>( EReservedSubstate::COUNT )
    };

	enum class ERetcode : uint8_t
	{
		SUCCESS = 0,
		ERROR_IO,
		ERROR_BAD_STATE
	};

	// Methods
	explicit StatusLights();

	void Initialize();
	void Post();
	void Update();

private:
	orutil::CTimer m_patternTimer;
	orutil::CTimer m_statusTimer;

	const uint32_t m_kStatusTimerPeriod_us = 1000000;

	bool m_patternOn = false;
	bool m_blinkState = false;

    uint8_t m_values = 0;

	const uint8_t m_powerBit = 0x00;
	const uint32_t m_powerCalcDelay_us = 450000;
	const uint32_t m_patternRate_us = 450000;

	uint8_t m_postAttempts = 0;

	uint8_t m_currentLED = 0;

	// From empirical tests, these are the percentage breakdowns to use
	// The Fuelgauge ususally cuts off at 30% remaining, so say that is zero
	const float m_percentRemainingLevels[5];

	// Sensor methods
	void CalculatePowerPattern();

	ERetcode ConfigureChip();
	ERetcode SetPowerPattern();
	ERetcode ClearPowerPattern();
	ERetcode CheckHealth();

	// State methods
	bool PostAttempt();

	void Update_Active();
	void Update_Standby();
	void Update_Recovery();
	void Update_Disabled();

	void Blink(int ledNumIn);
	void TurnOn(int ledNumIn);
	void TurnOff(int ledNumIn);
	void LowBattChargingBlink();
};
