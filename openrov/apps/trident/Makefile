# name of your application
APPLICATION = trident

# If no BOARD is found in the environment, use this default:
BOARD?=000001-02

# This has to be the absolute path to the RIOT base directory:
RIOTBASE ?= $(CURDIR)/../../..

CFG?=release

ifeq ($(CFG),release)
CFLAGS += -DLOG_LEVEL=LOG_NONE
endif

ifeq ($(CFG),debug)
CFLAGS += -DDEVELHELP -DLOG_LEVEL=LOG_INFO
endif

ifneq ($(CFG),debug)
ifneq ($(CFG),release)
$(error Bad build configuration.  Choices are debug, release, coverage.)
endif
endif

# Change this to suit debugging needs
CFLAGS_OPT = -O3 -Wall -Wextra -Werror

# RIOT modules
USEMODULE += xtimer

# Packages
USEPKG += mavlink

# Libraries
USEMODULE += i2cplus
USEMODULE += orutil

# Mavutils: Application configuration for mavlink interactions
# Total messages = MAVLINK_COMM_NUM_BUFFERS * 2 + MAVLINK_COMM_NUM_BUFFERS * MAVLINK_APP_MAX_MESSAGES_PER_CHANNEL
# Total required memory = total messages * 263 bytes
# Our default configuration ends up using ~6Kb of SRAM for buffering purposes
# Make sure data and bss usage is well below what is allowed
CXXEXFLAGS += -DMAVLINK_APP_MAX_MESSAGES_PER_CHANNEL=4
USEMODULE += mavutils

# Device drivers
USEMODULE += ms5837_30ba
USEMODULE += mpl3115
USEMODULE += pca9538

USEMODULE += bno055
USEMODULE += bno080

USEMODULE += bq34z100g1
USEMODULE += samd21_wdt

# If you want to add some extra flags when compiling C++ files, add these flags
# to CXXEXFLAGS variable
CXXEXFLAGS += -std=c++11 -fno-exceptions -fno-non-call-exceptions -fno-asynchronous-unwind-tables \
-fno-rtti -fno-use-cxa-atexit -ffreestanding -fno-common -Wall -Wextra -Werror -Wpedantic -fno-permissive

# Embed version string into firmware
CXXEXFLAGS += -DGIT_HASH='"$(GIT_HASH)"' -DBOARD_ID='"$(BOARD_ID)"' -DAPP_ID='"$(APP_ID)"'

# Change this to 0 show compiler invocation lines by default:
QUIET ?= 1

include $(RIOTBASE)/Makefile.include
