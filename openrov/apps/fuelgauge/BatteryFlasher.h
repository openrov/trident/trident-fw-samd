#pragma once

#include <stdlib.h>
// Includes
#include "bq34z100_interface.h"

#include "I2CMonitor.h"
#include "Subsystem.h"

class BatteryFlasher
{
    public: 
        enum class ESubstate : uint8_t
        {
            COMPARING = static_cast<uint8_t>( EReservedSubstate::COUNT ),
            FINISHED_PARSING,
            PARSING,
            READY_FOR_FLASH_COMMAND,
            FLASHING_SUCCESS,
            WAITING,
            WRITING,
            FAILURE
        };

        enum class ERetCode : uint8_t
        {
            SUCCESS = 0,
            ERROR_IO,
            ERROR_FAILED_COMPARE,
            ERROR_UNRECOGNIZED_FIRMWARE_COMMAND,
            ERROR_INDEX_OVERFLOW,
            ERROR_COMPARE_PARSE,
            ERROR_WRITE_PARSE,
            ERROR_WAIT_TIME_PARSE,
            ERROR_FAILED_RESET,
            ERROR_FAILED_IT_TRACKING,
            ERROR_FAILED_SEALING
        };

        // Methods
        explicit BatteryFlasher(I2C* i2cInterfaceIn);

        void Initialize();
        void Post();
        void Update();

    private:
        // I2C Driver
        I2C* m_i2c;
        
        // Subsystem for updates
        Subsystem m_subsystem;

        // Max amount of failures for i2c
        // TODO: Figure out an actual fail limit
        static constexpr uint8_t MAX_I2C_FAILURES = 25;

        // Booleans for checks
        bool m_startFlashing = false;
        bool m_shouldSkipEnterROM = false;

        // Mavlink stats
        mavlink_orov_batt_flash_status_t m_flasherStatus;
        void SetFlasherLastError(const OROV_BATT_FLASH_ERROR& errorIn);
        void SetFlasherSubstate(const OROV_BATT_FLASH_SUBSTATE& stateIn);
        void SetFlasherMainState(const OROV_SUBSYSTEM_STATE& stateIn);

        mavlink_orov_fuelgauge_status_t m_fuelgaugeStatus;
        uint16_t m_voltage_mv;
        int16_t m_avgCurrent_ma;
        int16_t m_insCurrent_ma;
        uint32_t m_avgPower_mw;
        uint8_t m_stateOfCharge_pct;
        uint16_t m_remainingCapacity_mah;
        uint32_t m_batteryTemperature_k;
        uint16_t m_flags;
        void ReadFuelgaugeValues();
        uint8_t m_firmwareVersion[2];

        void SendStatusUpdate();
        void SkipROMMode(){m_shouldSkipEnterROM = true;}
        // Precheck method (tries to read the firmware values)
        bool DevicePrecheck();

        // Post method
        bool PostAttempt();

        // Mavlink message handler
        void HandleMessages();

        // start the flashing process
        void StartFlashing();

        // Update methods
        void UpdateActive();

        // TI Flashstream specific methods
        ERetCode HandleCompare();
        ERetCode HandleParse();
        ERetCode HandleWait(uint32_t& waitData);
        ERetCode HandleWrite();

        ERetCode FinishFlashing();

        // Helpers
        const char* GetSubstate(const uint8_t& substateIn);

};
