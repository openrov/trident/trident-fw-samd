#pragma once

#include <i2cplus.h>
#include <log.h>

namespace bq34z100g1
{
    // I2C Slave Address
    namespace address
    {
        constexpr uint8_t A = 0x55;
    }

    enum class ECommand : uint8_t
    {
       CONTROL                  = 0x00,
       STATE_OF_CHARGE          = 0x02,
       MAX_ERROR                = 0x03,
       REMAINING_CAPACITY       = 0x04,
       FULL_CHARGE_CAPACITY     = 0x06,
       VOLTAGE                  = 0x08,
       AVERAGE_CURRENT          = 0x0A,
       TEMPERATURE              = 0x0C,
       FLAGS                    = 0x0E,
       CURRENT                  = 0x10,
       FLAGSB                   = 0x12
    };

    // Note: not the full set
    enum class EExtendedCommand : uint8_t
    {
       AVERAGE_TIME_TO_EMPTY    = 0x18,
       AVERAGE_TIME_TO_FULL     = 0x1A,
       PASSED_CHARGE            = 0x1C,
       DOD0_TIME                = 0x1E,
       AVAILABLE_ENERGY         = 0x24,
       AVERAGE_POWER            = 0x26,
       SERIAL_NUMBER            = 0x28,
       INTERNAL_TEMPERATURE     = 0x2A,
       CYCLE_COUNT              = 0x2C,
       STATE_OF_HEALTH          = 0x2E,
       CHARGE_VOLTAGE           = 0x30,
       CHARGE_CURRENT           = 0x32,
       DESIGN_CAPACITY          = 0x3A,
       LEARNED_STATUS           = 0x63
    };

    // Note: not the full set
    enum class EControlSubcommand : uint8_t
    {
        CONTROL_STATUS      = 0x00,
        DEVICE_TYPE         = 0x01,
        FW_VERSION          = 0x02,
        HW_VERSION          = 0x03,
        RESET_DATA          = 0x05,
        PREV_MACWRITE       = 0x07,
        CHEM_ID             = 0x08,
        BOARD_OFFSET        = 0x09,
        CC_OFFSET           = 0x0A,
        CC_OFFSET_SAVE      = 0x0B,
        DF_VERSION          = 0x0C,
        SET_FULLSLEEP       = 0x10,
        STATIC_CHEM_CHKSUM  = 0x17
    };

    // Reads two bytes after issuing command
    inline i2c_retcode_t ReadCommandBytes( I2C* dev, ECommand commandIn, uint8_t* dataOut )
    {
        return dev->ReadRegisterBytes( address::A, static_cast<uint8_t>( commandIn ), dataOut, 2 );
    }

    // Reads two bytes after issuing control command and subcommand
    inline i2c_retcode_t ReadControlSubcommandBytes( I2C* dev, EControlSubcommand subcommandIn, uint8_t* dataOut )
    {
        // Issue a control command with the specified subcommand
        uint8_t subcommand[2] = { static_cast<uint8_t>( subcommandIn ), 0x00 };
        auto ret = dev->WriteRegisterBytes( address::A, static_cast<uint8_t>( ECommand::CONTROL ), subcommand, 2 );
        if( ret )
        {
            return ret;
        }

        // Read the result of the subcommand
        return dev->ReadRegisterBytes( address::A, static_cast<uint8_t>( ECommand::CONTROL ), dataOut, 2 );
    }

    // Methods
    inline i2c_retcode_t ReadVoltage( I2C* dev, uint16_t &voltageOut_mv )
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes( dev, ECommand::VOLTAGE, data );
        if( ret ){ return ret; }

        // Combine bytes
        voltageOut_mv = ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ];
        return ret;
    }

    inline i2c_retcode_t ReadAverageCurrent( I2C* dev, int16_t &currentOut_ma )
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes( dev, ECommand::AVERAGE_CURRENT, data );
        if( ret ){ return ret; }

        // Combine bytes
        currentOut_ma = static_cast<int16_t>( ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ] );
        return ret;
    }

    inline i2c_retcode_t ReadInstantCurrent( I2C* dev, int16_t &currentOut_ma )
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes( dev, ECommand::CURRENT, data );
        if( ret ){ return ret; }

        // Combine bytes
        currentOut_ma = static_cast<int16_t>( ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ] );
        return ret;
    }

    inline i2c_retcode_t ReadAveragePower( I2C* dev, uint32_t &powerOut_mw )
    {
        uint8_t data[2];
        auto ret = dev->ReadRegisterBytes( address::A, static_cast<uint8_t>( EExtendedCommand::AVERAGE_POWER ), data, 2 );
        if( ret ){ return ret; }

        // Unit = 10mW, divide by 10
        powerOut_mw = ( ( (uint32_t)data[ 1 ] << 8 ) | data[ 0 ] ) / 10;
        return ret;
    }

    inline i2c_retcode_t ReadStateOfCharge( I2C* dev, uint8_t &percentOut )
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes( dev, ECommand::STATE_OF_CHARGE, data );
        if( ret ){ return ret; }

        // Percent is LSB
        percentOut = data[ 0 ];
        return ret;
    }

    // TODO: configure  Cell Termination Voltage, this will read 0 until then
    inline i2c_retcode_t ReadRemainingCapacity( I2C* dev, uint16_t &capacityOut_mah )
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes( dev, ECommand::REMAINING_CAPACITY, data );
        if( ret ){ return ret; }

        capacityOut_mah = ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ];
        return ret;
    }

    inline i2c_retcode_t ReadTemperature( I2C* dev, uint32_t &temperatureOut_k )
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes( dev, ECommand::TEMPERATURE, data );
        if( ret ){ return ret; }

        // Unit = 0.1K, multiply by 10
        temperatureOut_k = ( ( (uint32_t)data[ 1 ] << 8 ) | data[ 0 ] ) * 10;
        return ret;
    }

    inline i2c_retcode_t ReadFlags( I2C* dev, uint16_t &flagsOut )
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes( dev, ECommand::FLAGS, data );
        if( ret ){ return ret; }

        flagsOut = ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ];
        return ret;
    }

    inline i2c_retcode_t ReadFullChargeCapacity( I2C* dev, uint16_t &fullChargeCapOut_mah )
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes( dev, ECommand::FULL_CHARGE_CAPACITY, data );
        if( ret ){ return ret; }

        fullChargeCapOut_mah = ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ];
        return ret;
    }

    // TODO: configure  Cell Termination Voltage, this will read 0 until then
    inline i2c_retcode_t ReadAverageTimeToEmpty( I2C* dev, uint16_t &timeToEmptyOut_mins )
    {
        uint8_t data[2];
        auto ret = dev->ReadRegisterBytes( address::A, static_cast<uint8_t>( EExtendedCommand::AVERAGE_TIME_TO_EMPTY ), data, 2 );
        if( ret ){ return ret; }

        timeToEmptyOut_mins = ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ];
        return ret;
    }

	inline i2c_retcode_t ReadCycleCount( I2C* dev, uint16_t &cycleCountOut )
    {
        uint8_t data[2];
        auto ret = dev->ReadRegisterBytes( address::A, static_cast<uint8_t>( EExtendedCommand::CYCLE_COUNT ), data, 2 );
        if( ret ){ return ret; }

        cycleCountOut = ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ];
        return ret;
    }

	inline i2c_retcode_t ReadStateOfHealth( I2C* dev, uint8_t &percentOut )
    {
        uint8_t data[2];
        auto ret = dev->ReadRegisterBytes( address::A, static_cast<uint8_t>( EExtendedCommand::STATE_OF_HEALTH ), data, 2 );
        if( ret ){ return ret; }

        percentOut = data[ 0 ];
        return ret;
    }

    inline i2c_retcode_t IsSealed( I2C* dev, bool &isSealedOut )
    {
        uint8_t data[2];
        auto ret = ReadControlSubcommandBytes( dev, EControlSubcommand::CONTROL_STATUS, data );
        if( ret ){ return ret; }

        // Return state of 6th bit, SS (sealed state)
        isSealedOut = ( data[1] & (1 << 5) );
        return ret;
    }

    inline i2c_retcode_t IsITEnabled( I2C* dev, bool &isITEnabledOut )
    {
        uint8_t data;
        auto ret = dev->ReadRegisterByte( address::A, static_cast<uint8_t>( EExtendedCommand::LEARNED_STATUS ), &data );
        if( ret ){ return ret; }

        // Return state of 3rd bit, ITEN (Impedance tracking enabled)
        isITEnabledOut = ( data & (1 << 2) );
        return ret;
    }
}