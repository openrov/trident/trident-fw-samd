#!/bin/bash
set -e

if [ "$UID" != 0 ] ; then
    echo "$0: This script needs to be run as root" 1>&2
    exit 1
fi

# Select SAMD on the SWD switch
/opt/openrov/firmware/scripts/000001-02/selectSAMD.sh

# Flash firmware
openocd -f /usr/share/openocd/scripts/board/openrov_trident_samd.cfg -c "program /opt/openrov/firmware/000001-02/samd21/empty.bin verify; reset; exit"
