#include <xtimer.h>
#include <board.h>

#include "SystemMonitor.h"
#include "CommLink.h"
#include "I2CMonitor.h"

#include <periph/gpio.h>
namespace
{
    // Subsystems
    CommLink        commLink;

    I2C I2CMain( I2C_0, i2c_speed_t::I2C_SPEED_FAST );

    void CyclePowerI2CMain()
    {
        LOG_DEBUG( "I2CMonitor::CyclePowerI2CMain()\n" );

        // Cycle power with 10ms between
        gpio_clear( I2C0_PWR_PIN );
        xtimer_usleep( 10000 );
        gpio_set( I2C0_PWR_PIN );
        xtimer_usleep( 10000 );
    }
}

void Initialize()
{
    LOG_DEBUG( "\n----- INITIALIZE -----\n" );
    // Next, initialize the commlink. If this fails for the main channel, we treat it as fatal and reset.
    commLink.Initialize();

    // Initialize the I2C
    CyclePowerI2CMain();
    I2CMain.Enable();

}

void Post()
{
    LOG_DEBUG( "\n----- POST -----\n" );

    // Handle the Power-on-self-test stage for all subsystems
    commLink.Post();
}

void Run()
{
    LOG_DEBUG( "\n----- RUN -----\n" );

    while( true )
    {
        // Receive messages from CPU
        commLink.Update();
    }
}

int main()
{
    xtimer_init();
    LOG_DEBUG("Starting Empty Firmware (quiet i2c) \n");
    Initialize();
    Post();

    // Infinite loop
    Run();

    return 0;
}
