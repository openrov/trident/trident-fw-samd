# Prereqs:
- gcc-arm-none-eabi
- gdb-arm-none-eabi

# Build:
- make clean
- make

# Or:
- make CFG=debug