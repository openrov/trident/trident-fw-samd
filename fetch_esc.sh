#!/bin/bash
set -ex

# Setup variables
BOARD="000001-02"
CPU="pac5223"
APP="trident_pac"
APP_GITHASH="dbaf76eb"

# Download ESC firmware file
# TODO: Use version number
wget https://s3-us-west-1.amazonaws.com/pacbin/pac.bin

# Install to v2/pac5223/ directory
mkdir -p build/opt/openrov/firmware/${BOARD}/${CPU}
cp pac.bin build/opt/openrov/firmware/${BOARD}/${CPU}/${APP}.bin

# Add manifest information
jq  --arg board "$BOARD" \
    --arg cpu "$CPU" \
    --arg app "$APP" \
    --arg hash "$APP_GITHASH" \
    '.[$board][$cpu][$app] |= $hash' manifest.json | sponge manifest.json