#include "SystemMonitor.h"

#include "CommLink.h"

// Static members
WatchdogTimer SystemMonitor::m_wdt;
Subsystem* SystemMonitor::mp_subsystems[ OROV_TRIDENT_SUBSYSTEM_ID::OROV_TRIDENT_SUBSYSTEM_ID_COUNT ];

// Static methods
void SystemMonitor::ResetWDT()
{
    m_wdt.Reset();
}

void SystemMonitor::RegisterSubsystem( Subsystem* subsystemIn )
{
    assert( subsystemIn != nullptr );
    assert( subsystemIn->GetId() < OROV_TRIDENT_SUBSYSTEM_ID_COUNT );
    
    mp_subsystems[ subsystemIn->GetId() ] = subsystemIn;
}

/////////////////////////////////////////////

SystemMonitor::SystemMonitor()
    : m_mcu{ OROV_TRIDENT_MCU_ID_SAMD21 }
{
}

void SystemMonitor::Initialize()
{
    // Initialize timer
    xtimer_init();

    // Initialize system status information for MCU
    CheckResetCause();

    // Initialize MCU status information
    m_mcu.SetFlashCRCResult( OROV_FLASH_CRC_RESULT_SUCCESS );   // TODO: Actually perform the flash check and set it here.
    m_mcu.SetMavlinkVersion( MAVLINK_VERSION );

    // Verify that githash and boardid defines are the correct length
    static_assert( sizeof( GIT_HASH ) == ( MAVLINK_MSG_OROV_MCU_STATUS_FIELD_VERSION_LEN ), "Firmware version string size does not match mavlink version field size!" );
    static_assert( sizeof( APP_ID ) <= ( MAVLINK_MSG_OROV_MCU_STATUS_FIELD_APP_ID_LEN ), "App ID string size is not <= mavlink version field size!" );
    static_assert( sizeof( BOARD_ID ) == ( MAVLINK_MSG_OROV_MCU_STATUS_FIELD_BOARD_ID_LEN ), "Board ID string size does not match mavlink version field size!" );
    
    m_mcu.SetVersion( GIT_HASH );
    m_mcu.SetAppId( APP_ID );
    m_mcu.SetBoardId( BOARD_ID );

    // Enable WDT for 4 second period, 2 second warning
    m_wdt.Enable( (uint8_t)EWatchdogPeriod::WDT_PERIOD_4096, (uint8_t)EWatchdogPeriod::WDT_PERIOD_2048 );

    m_timer_1hz.Reset();
    m_timer_10hz.Reset();
}

void SystemMonitor::Update()
{
    HandleMessages();

    // Handle WDT early warning
    if( m_wdt.earlyWarningTrigger )
    {
        m_wdt.earlyWarningTrigger = false;

        m_wdtWarning.count = m_wdt.earlyWarningCount;
        mavlink_msg_orov_wdt_warning_send_struct( CommLink::MAIN_CHANNEL, &m_wdtWarning );
    }

    // Handle loop rate update
    if( m_timer_1hz.HasElapsed( 1000000 ) )
    {
        // Reset WDT
        m_wdt.Reset();

        // Update MCU uptime metrics
        xtimer_now_timex( &m_currentTime );
        m_mcu.SetUptimeSecs( m_currentTime.seconds );

        // Report all MCU and subsystem info
        ReportMCUStatus();
        ReportSubsystemStatus();
    }
}

void SystemMonitor::HandleMessages()
{
    int messageId = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessageID();
    
    switch( messageId )
    {
		case MAVLINK_MSG_ID_OROV_PING:
        {
			mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();
            m_ping.sequence = mavlink_msg_orov_ping_get_sequence( message );
			mavlink_msg_orov_ping_send_struct( CommLink::MAIN_CHANNEL, &m_ping );
			break;
        }
        
        // TODO: Explicit Version request handling, instead of constant publishing

		default:
		{
			break;
		}
	}
}

void SystemMonitor::ReportMCUStatus()
{
    m_mcu.SendStatus( CommLink::MAIN_CHANNEL );
}

void SystemMonitor::ReportSubsystemStatus()
{
    // Initialize subsystem status IDs
    for( uint8_t i = 0; i < OROV_TRIDENT_SUBSYSTEM_ID_COUNT; ++i )
    {
        if( mp_subsystems[ i ] == nullptr ){ continue; }

        mavlink_msg_orov_subsystem_status_send_struct( CommLink::MAIN_CHANNEL, mp_subsystems[ i ]->GetStatus() );
        mp_subsystems[ i ]->Debug_ReportStatus();
    }
}

void SystemMonitor::CheckResetCause()
{
    uint8_t cause = OROV_MCU_RESET_CAUSE_UNKNOWN;
    uint8_t extra = 0;

    if( PM->RCAUSE.bit.SYST )                       { cause = OROV_MCU_RESET_CAUSE_SOFT_RESET; }
    else if( PM->RCAUSE.reg & PM_RCAUSE_WDT )       { cause = OROV_MCU_RESET_CAUSE_WATCHDOG; }          // Special case due to syntax error in CMSIS definitions
    else if( PM->RCAUSE.bit.EXT )                   { cause = OROV_MCU_RESET_CAUSE_EXTERNAL; }
    else if( PM->RCAUSE.bit.BOD33 )                 { cause = OROV_MCU_RESET_CAUSE_BROWNOUT; extra = 0; }
    else if( PM->RCAUSE.bit.BOD12 )                 { cause = OROV_MCU_RESET_CAUSE_BROWNOUT; extra = 1; }
    else if( PM->RCAUSE.bit.POR )                   { cause = OROV_MCU_RESET_CAUSE_POWER_ON_RESET; }

    m_mcu.SetResetCause( cause );
    m_mcu.SetResetExtra( extra );
}