/*
 * Copyright (C) 2015 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

 /*
    DFLL48M Clock Configuration Code modified from Arduino Zero
    SAMD core: ArduinoCore-samd/cores/arduino/startup.c

    Copyright (c) 2015 Arduino LLC.  All right reserved.
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * @ingroup     cpu_samd21
 * @{
 *
 * @file
 * @brief       Implementation of the CPU initialization
 *
 * @author      Thomas Eichinger <thomas.eichinger@fu-berlin.de>
 * @author      Hauke Petersen <hauke.petersen@fu-berlin.de>
 * @}
 */

#include "cpu.h"
#include "periph_conf.h"


/**
 * @brief   Configure clock sources and the cpu frequency
 */
static void clk_init(void)
{
    /* adjust NVM wait states, see table 42.30 (p. 1070) in the datasheet */
    #if (CLOCK_CORECLOCK > 24000000)
        PM->APBAMASK.reg |= PM_AHBMASK_NVMCTRL;
        NVMCTRL->CTRLB.reg |= NVMCTRL_CTRLB_RWS(1);
        PM->APBAMASK.reg &= ~PM_AHBMASK_NVMCTRL;
    #endif

    /* Turn on the digital interface clock */
    PM->APBAMASK.reg |= PM_APBAMASK_GCLK;

    #if CLOCK_USE_XOSC32_WITH_DFLL48
        /**
        * At reset:
        * - OSC8M clock source is enabled with a divider by 8 (1MHz).
        * - Generic Clock Generator 0 (GCLKMAIN) is using OSC8M as source.
        * We need to:
        * 1) Enable XOSC32K clock (External on-board 32.768Hz oscillator), will be used as DFLL48M reference.
        * 2) Put XOSC32K as source of Generic Clock Generator 1
        * 3) Put Generic Clock Generator 1 as source for Generic Clock Multiplexer 0 (DFLL48M reference)
        * 4) Enable DFLL48M clock
        * 5) Switch Generic Clock Generator 0 to DFLL48M. CPU will run at 48MHz.
        * 6) Put OSC8M at 1MHz as source for Generic Clock Generator 3
        */

        // ----------------------------------------------------------------------------------------------
        // 1) Enable XOSC32K clock (External on-board 32.768Hz oscillator)
        SYSCTRL->XOSC32K.reg    = SYSCTRL_XOSC32K_STARTUP( 0x6u )   /* cf table 15.10 of product datasheet in chapter 15.8.6 */
                                | SYSCTRL_XOSC32K_XTALEN 
                                | SYSCTRL_XOSC32K_EN32K;

        // Enable XOSC32K; separate call, as described in chapter 15.6.3
        SYSCTRL->XOSC32K.bit.ENABLE = 1 ;

        // Wait for oscillator stabilization
        while ( (SYSCTRL->PCLKSR.reg & SYSCTRL_PCLKSR_XOSC32KRDY) == 0 ){}

        /* Software reset the module to ensure it is re-initialized correctly */
        /* Note: Due to synchronization, there is a delay from writing CTRL.SWRST until the reset is complete.
        * CTRL.SWRST and STATUS.SYNCBUSY will both be cleared when the reset is complete, as described in chapter 13.8.1
        */
        GCLK->CTRL.reg = GCLK_CTRL_SWRST ;

        while ( (GCLK->CTRL.reg & GCLK_CTRL_SWRST) && (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) )
        {
            /* Wait for reset to complete */
        }

        /* ----------------------------------------------------------------------------------------------
        * 2) Put XOSC32K as source of Generic Clock Generator 1
        */
        GCLK->GENDIV.reg = GCLK_GENDIV_ID( GENERIC_CLOCK_GENERATOR_XOSC32K ) ; // Generic Clock Generator 1

        // Sync
        while ( GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY ){}

        /* Write Generic Clock Generator 1 configuration */
        GCLK->GENCTRL.reg = GCLK_GENCTRL_ID( GENERIC_CLOCK_GENERATOR_XOSC32K ) | // Generic Clock Generator 1
                            GCLK_GENCTRL_SRC_XOSC32K | // Selected source is External 32KHz Oscillator
                            GCLK_GENCTRL_GENEN ;

        // Sync
        while ( GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY ){}

        /* ----------------------------------------------------------------------------------------------
        * 3) Put Generic Clock Generator 1 as source for Generic Clock Multiplexer 0 (DFLL48M reference)
        */
        GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID( GENERIC_CLOCK_MULTIPLEXER_DFLL48M ) | // Generic Clock Multiplexer 0
                            GCLK_CLKCTRL_GEN_GCLK1 | // Generic Clock Generator 1 is source
                            GCLK_CLKCTRL_CLKEN ;

        while ( GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY ){}

        /* ----------------------------------------------------------------------------------------------
        * 4) Enable DFLL48M clock
        */

        /* DFLL Configuration in Closed Loop mode, cf product datasheet chapter 15.6.7.1 - Closed-Loop Operation */

        /* Remove the OnDemand mode, Bug http://avr32.icgroup.norway.atmel.com/bugzilla/show_bug.cgi?id=9905 */
        SYSCTRL->DFLLCTRL.bit.ONDEMAND = 0 ;

        while ( (SYSCTRL->PCLKSR.reg & SYSCTRL_PCLKSR_DFLLRDY) == 0 )
        {
            /* Wait for synchronization */
        }

        SYSCTRL->DFLLMUL.reg = SYSCTRL_DFLLMUL_CSTEP( 31 ) | // Coarse step is 31, half of the max value
                                SYSCTRL_DFLLMUL_FSTEP( 511 ) | // Fine step is 511, half of the max value
                                SYSCTRL_DFLLMUL_MUL( (CLOCK_CORECLOCK/CLOCK_XOSC32K) ) ; // External 32KHz is the reference

        while ( (SYSCTRL->PCLKSR.reg & SYSCTRL_PCLKSR_DFLLRDY) == 0 )
        {
            /* Wait for synchronization */
        }

        /* Write full configuration to DFLL control register */
        SYSCTRL->DFLLCTRL.reg |= SYSCTRL_DFLLCTRL_MODE | /* Enable the closed loop mode */
                                SYSCTRL_DFLLCTRL_WAITLOCK |
                                SYSCTRL_DFLLCTRL_QLDIS ; /* Disable Quick lock */

        while ( (SYSCTRL->PCLKSR.reg & SYSCTRL_PCLKSR_DFLLRDY) == 0 )
        {
            /* Wait for synchronization */
        }

        /* Enable the DFLL */
        SYSCTRL->DFLLCTRL.reg |= SYSCTRL_DFLLCTRL_ENABLE ;

        while ( (SYSCTRL->PCLKSR.reg & SYSCTRL_PCLKSR_DFLLLCKC) == 0 ||
                (SYSCTRL->PCLKSR.reg & SYSCTRL_PCLKSR_DFLLLCKF) == 0 )
        {
            /* Wait for locks flags */
        }

        while ( (SYSCTRL->PCLKSR.reg & SYSCTRL_PCLKSR_DFLLRDY) == 0 )
        {
            /* Wait for synchronization */
        }

        /* ----------------------------------------------------------------------------------------------
        * 5) Switch Generic Clock Generator 0 to DFLL48M. CPU will run at 48MHz.
        */
        GCLK->GENDIV.reg = GCLK_GENDIV_ID( GENERIC_CLOCK_GENERATOR_MAIN ) ; // Generic Clock Generator 0

        while ( GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY )
        {
            /* Wait for synchronization */
        }

        /* Write Generic Clock Generator 0 configuration */
        GCLK->GENCTRL.reg = GCLK_GENCTRL_ID( GENERIC_CLOCK_GENERATOR_MAIN ) | // Generic Clock Generator 0
                            GCLK_GENCTRL_SRC_DFLL48M | // Selected source is DFLL 48MHz
        //                      GCLK_GENCTRL_OE | // Output clock to a pin for tests
                            GCLK_GENCTRL_IDC | // Set 50/50 duty cycle
                            GCLK_GENCTRL_GENEN ;

        while ( GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY )
        {
            /* Wait for synchronization */
        }

        /* ----------------------------------------------------------------------------------------------
        * 6) Put OSC8M as source for Generic Clock Generator 3
        */
        SYSCTRL->OSC8M.bit.ONDEMAND = 0 ;
        GCLK->GENDIV.reg = GCLK_GENDIV_ID( GENERIC_CLOCK_GENERATOR_OSC8M ) ; // Generic Clock Generator 3

        /* Write Generic Clock Generator 3 configuration */
        GCLK->GENCTRL.reg = GCLK_GENCTRL_ID( GENERIC_CLOCK_GENERATOR_OSC8M ) | // Generic Clock Generator 3
                            GCLK_GENCTRL_SRC_OSC8M | // Selected source is RC OSC 8MHz (already enabled at reset)
        //                      GCLK_GENCTRL_OE | // Output clock to a pin for tests
                            GCLK_GENCTRL_GENEN ;

        while ( GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY )
        {
            /* Wait for synchronization */
        }

        /* ----------------------------------------------------------------------------------------------
        * 7) Set up clock generator 2 for WDT and other system peripherals
        */

         // Generic clock generator 2, divisor = 32 (2^(DIV+1))
        GCLK->GENDIV.reg = GCLK_GENDIV_ID( GENERIC_CLOCK_GENERATOR_OSCULP32K ) | GCLK_GENDIV_DIV( 4 );

        // Enable clock generator 2 using low-power 32KHz oscillator.
        // With /32 divisor above, this yields 1024Hz(ish) clock.
        GCLK->GENCTRL.reg = GCLK_GENCTRL_ID( GENERIC_CLOCK_GENERATOR_OSCULP32K ) |
                            GCLK_GENCTRL_GENEN |
                            GCLK_GENCTRL_SRC_OSCULP32K |
                            GCLK_GENCTRL_DIVSEL;
                            
        while( GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY )
        {
            /* Wait for synchronization */
        }

    #else
        /* configure internal 8MHz oscillator to run without prescaler */
        SYSCTRL->OSC8M.bit.PRESC = 0;
        SYSCTRL->OSC8M.bit.ONDEMAND = 0;
        SYSCTRL->OSC8M.bit.RUNSTDBY = 0;
        SYSCTRL->OSC8M.bit.ENABLE = 1;
        while (!(SYSCTRL->PCLKSR.reg & SYSCTRL_PCLKSR_OSC8MRDY)) {}

        #if CLOCK_USE_PLL
            /* reset the GCLK module so it is in a known state */
            GCLK->CTRL.reg = GCLK_CTRL_SWRST;
            while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) {}

            /* setup generic clock 1 to feed DPLL with 1MHz */
            GCLK->GENDIV.reg = (GCLK_GENDIV_DIV(8) |
                                GCLK_GENDIV_ID(1));

            GCLK->GENCTRL.reg = (GCLK_GENCTRL_GENEN |
                                GCLK_GENCTRL_SRC_OSC8M |
                                GCLK_GENCTRL_ID(1));

            GCLK->CLKCTRL.reg = (GCLK_CLKCTRL_GEN(1) |
                                GCLK_CLKCTRL_ID(1) |
                                GCLK_CLKCTRL_CLKEN);

            while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) {}

            /* enable PLL */
            SYSCTRL->DPLLRATIO.reg = (SYSCTRL_DPLLRATIO_LDR(CLOCK_PLL_MUL));
            SYSCTRL->DPLLCTRLB.reg = (SYSCTRL_DPLLCTRLB_REFCLK_GCLK);
            SYSCTRL->DPLLCTRLA.reg = (SYSCTRL_DPLLCTRLA_ENABLE);
            while(!(SYSCTRL->DPLLSTATUS.reg &
                (SYSCTRL_DPLLSTATUS_CLKRDY | SYSCTRL_DPLLSTATUS_LOCK))) {}

            /* select the PLL as source for clock generator 0 (CPU core clock) */
            GCLK->GENDIV.reg =  (GCLK_GENDIV_DIV(CLOCK_PLL_DIV) |
                                GCLK_GENDIV_ID(0));
            GCLK->GENCTRL.reg = (GCLK_GENCTRL_GENEN |
                                GCLK_GENCTRL_SRC_FDPLL |
                                GCLK_GENCTRL_ID(0));
        #else /* do not use PLL, use internal 8MHz oscillator directly */
            GCLK->GENDIV.reg =  (GCLK_GENDIV_DIV(CLOCK_DIV) |
                                GCLK_GENDIV_ID(0));
            GCLK->GENCTRL.reg = (GCLK_GENCTRL_GENEN |
                                GCLK_GENCTRL_SRC_OSC8M |
                                GCLK_GENCTRL_ID(0));
        #endif

        /* make sure we synchronize clock generator 0 before we go on */
        while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) {}

        /* redirect all peripherals to a disabled clock generator (7) by default */
        for (int i = 0x3; i <= 0x22; i++) {
            GCLK->CLKCTRL.reg = ( GCLK_CLKCTRL_ID(i) | GCLK_CLKCTRL_GEN_GCLK7 );
            while (GCLK->STATUS.bit.SYNCBUSY) {}
        }
    #endif
}

void cpu_init(void)
{
    /* disable the watchdog timer */
    WDT->CTRL.bit.ENABLE = 0;
    /* initialize the Cortex-M core */
    cortexm_init();
    /* Initialise clock sources and generic clocks */
    clk_init();
}
