#pragma once

#include <i2cplus.h>
#include "I2CDevice.h"
#include "Subsystem.h"


class I2CMonitor
{
public:
    enum class ERetcode : uint8_t
    {
        SUCCESS = 0,
        ERROR_BUS_LATCHED,
        ERROR_DRIVER_FAILURE
    };

    // I2C peripheral instances
    static I2C I2CMain;
    
    // Methods
    I2CMonitor()
        : m_subsystem{ OROV_TRIDENT_SUBSYSTEM_ID_I2C_MONITOR, "I2CM" }
    {
    }

    void Initialize();
    void Post();
    void Update();
    
    static bool doHardReset;
    static bool re_init;
    void HardReset();

    // Method used by devices to register themselves with the I2C Monitor
    static void RegisterDevice( I2CDevice* deviceIn )
    {
        assert( deviceIn != nullptr );
        assert( deviceIn->GetId() < EI2CDeviceId::COUNT );

        if( m_devices[ static_cast<uint8_t>( deviceIn->GetId() ) ] != nullptr )
        {
            LOG_DEBUG( "I2CMonitor::RegisterDevice(): Device already registered!\n" );
            return;
        }
        
        m_devices[ static_cast<uint8_t>( deviceIn->GetId() ) ] = deviceIn;
        m_registeredDevices++;
    }

private:
    // I2C Devices
    Subsystem m_subsystem;
    static I2CDevice* m_devices[ static_cast<uint8_t>( EI2CDeviceId::COUNT ) ];
    static int m_registeredDevices;

    mavlink_orov_i2c_monitor_stats_t m_stats;

    bool m_shouldPerformHardReset = false;


    // Subsystem attributes
    orutil::CTimer  m_monitorTimer;
    orutil::CTimer  m_resetTimer;
    orutil::CTimer  m_reportTimer;

    // Methods
    bool DeviceFailureLimitsReached();
    bool BusFailureOccurred();

    void CyclePowerI2CMain();
    void SendEvent( EI2CEvent eventIn );

    void ReportStats();

    //void HardReset();
    void SoftReset();
};
