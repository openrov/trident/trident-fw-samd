#include <mpl3115.h>

namespace mpl3115
{
    uint8_t g_data[5];
    uint8_t g_status = 0;
    uint8_t g_settings = 0;
    uint8_t g_msb = 0;
    uint8_t g_csb = 0;
    uint8_t g_lsb = 0;
    uint32_t g_totalPressure = 0;
}