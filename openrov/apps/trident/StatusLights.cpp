#include "StatusLights.h"
#include "NState.h"

#include "SystemMonitor.h"
#include "CommLink.h"

///////////////////////////////////////////////////////////////////

StatusLights::StatusLights()
    : I2CDevice( EI2CDeviceId::PCA9538, 
                 OROV_TRIDENT_SUBSYSTEM_ID_STATUS_LIGHTS,
                 "SLIG", 10)
	, m_percentRemainingLevels{0.39f, 0.49f, 0.74f, 0.92f}
{
}

void StatusLights::Initialize()
{
	// Register with SystemMonitor
	SystemMonitor::RegisterSubsystem( &m_subsystem );

	// Register I2C device with monitor
	I2CMonitor::RegisterDevice( this );

	// Reset Timer
	m_patternTimer.Reset();
	m_statusTimer.Reset();

	// Update state
	m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_INITIALIZED );
	m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
}

void StatusLights::Post()
{
	// Reset state and inform that the post result is being awaited
	m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_POSTING);
	m_subsystem.SetPostResult( OROV_POST_RESULT_WAITING );

	// Check I2C availability
	if( m_i2c->GetState() != EI2CState::READY )
	{
		// Set error code and result
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
		m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
		return;
	}

	int postCount 	= 0;
	bool postResult = false;

	do
	{
		// Reset the WDT on each attempt
		SystemMonitor::ResetWDT();

		// Make an attempt to post
		postResult = PostAttempt();
		postCount++;
	}
	while( !postResult && postCount < 3 );

	if( postResult )
	{   // Success
		m_subsystem.SetPostResult( OROV_POST_RESULT_SUCCESS );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
		m_subsystem.TransitionSubstate( ESubstate::POWER_PATTERN );
	}
	else
	{   // Failure
		m_subsystem.SetPostResult( OROV_POST_RESULT_FAILURE );

		// Update state
		m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
		m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
	}
}

bool StatusLights::PostAttempt()
{
    // Configure the chip in its initial all off position, except the power light
    auto ret = ConfigureChip();
    if( ret != ERetcode::SUCCESS ){ return false; }

	return true;
}

void StatusLights::Update()
{
	switch( m_subsystem.GetState() )
	{
		case OROV_SUBSYSTEM_STATE_ACTIVE:	{ Update_Active(); 		break; }
		case OROV_SUBSYSTEM_STATE_RECOVERY:	{ Update_Recovery();	break; }
		case OROV_SUBSYSTEM_STATE_STANDBY:	{ Update_Standby();		break; }
		case OROV_SUBSYSTEM_STATE_DISABLED:	{ break; }

		default: 
		{	// Move to the recovery state. These states aren't supported
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void StatusLights::Update_Active()
{
	if( m_statusTimer.HasElapsed( m_kStatusTimerPeriod_us ) )
	{
		// Check Health
		if( CheckHealth() != ERetcode::SUCCESS )
		{
			// Go to recovery mode and restart the sensor
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
		}
	}

	// Always calculate the new pattern
	if( m_patternTimer.HasElapsed( m_powerCalcDelay_us ) )
	{
		CalculatePowerPattern();
	}

	switch( m_subsystem.GetSubstate() )
	{
		case static_cast<uint8_t>( EReservedSubstate::DELAY ):
		{
			m_subsystem.TryDelayedTransition();
			break;
		}

		case static_cast<uint8_t>( ESubstate::POWER_PATTERN ):
		{
			// if( m_patternOn )
			// {
				// Leave lights always on, by request
				SetPowerPattern();
				// m_patternOn = false;
			// }
			// else
			// {
			// 	ClearPowerPattern();
			// 	m_patternOn = true;
			// }

			m_subsystem.TransitionSubstateDelayed( ESubstate::POWER_PATTERN, m_patternRate_us );
			break;
		}

		default:
		{   // Shouldn't get here
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void StatusLights::Update_Standby()
{
	// Check to see if I2C bus is back
	if( m_i2c->Available() )
	{
		// Restore the original state
		m_subsystem.RestoreStateAndSubstate();
	}
}

void StatusLights::Update_Recovery()
{
	switch( m_subsystem.GetSubstate() )
	{
		case static_cast<uint8_t>( EReservedSubstate::DELAY ):
		{
			m_subsystem.TryDelayedTransition();
			break;
		}

		case static_cast<uint8_t>( EReservedSubstate::RESET ):
		{
			// Reset and reconfigure in the specified period
			auto ret = ConfigureChip();
    		if( ret != ERetcode::SUCCESS )
			{
				m_subsystem.TransitionSubstateDelayed( EReservedSubstate::RESET, 3000000 );
			}
			else
			{
				m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_ACTIVE );
            	m_subsystem.TransitionSubstate( ESubstate::POWER_PATTERN );
			}

			break;
		}

		default:
		{
			// Shouldn't get here
			m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
			m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
			break;
		}
	}
}

void StatusLights::TurnOn(int ledNumIn)
{
	switch(ledNumIn)
	{
		case(0):
		{
			// Four Green On - Red Off
			m_values = ~(m_powerBit) & ~(0x1E);
			break;
		}
		case(1):
		{
			// Three Green On - Red Off
			// X O O O X
			m_values = ~(m_powerBit) & ~(0xE);
			break;
		}
		case(2):
		{
			// Two Green On - Red Off
			// X X O O X
			m_values = ~(m_powerBit) & ~(0x6);
			break;
		}
		case(3):
		{
			// One Green On - Red Off
			// X X X O X
			m_values = ~(m_powerBit) & ~(0x2);
			break;
		}
		case(4):
		{
			// Red is the only one on
			// X X X X O
			m_values = ~(m_powerBit) & ~(0x1);
			break;
		}
		default:
		{
			// All on
			m_values = ~(m_powerBit) & ~(0x1E);
			break;
		}
	}
}
void StatusLights::TurnOff(int ledNumIn)
{
	switch(ledNumIn)
	{
		case(0):
		{
			// First one from port is off
			m_values = ~(m_powerBit) & ~(0xE);
			break;
		}
		case(1):
		{
			m_values = ~(m_powerBit) & ~(0x6);
			break;
		}
		case(2):
		{
			m_values = ~(m_powerBit) & ~(0x2);
			break;
		}
		case(3):
		{
			m_values = ~(m_powerBit) & ~(0x1E);
			break;
		}
		case(4):
		{
			// All off
			m_values = 0xFF;
			break;
		}
		default:
		{
			// All off
			m_values = ~(m_powerBit) & ~(0x1E);
			break;
		}
	}
}

void StatusLights::Blink(int ledNumIn)
{
	if(m_blinkState)
	{
		TurnOn(ledNumIn);
		m_blinkState = false;
	}
	else
	{
		TurnOff(ledNumIn);
		m_blinkState = true;
	}
}

void StatusLights::LowBattChargingBlink()
{
	// Special case of blinking
	// Red is on, Blink 1 green
	if(m_blinkState)
	{
		// Turn on red and green
		m_values = ~(0x01) & ~(0x3);
		m_blinkState = false;
	}
	else
	{
		// Turn off green, leave red on
		m_values = ~(0x01) /*& ~(0x1)*/;
		m_blinkState = true;
	}

}

void StatusLights::CalculatePowerPattern()
{
	if(state::remainingPower <= m_percentRemainingLevels[0])
	{
		// LOW BATT
		// 0 <= p <= 30
		if(state::discharging)
		{
			// Discharging, Blink RED/ Off Greens
			Blink(4);
		}
		else
		{
			// Charging, RED On / Blink 1 Green
			//Blink(3);
			LowBattChargingBlink();
		}
	}
	else if(state::remainingPower > m_percentRemainingLevels[0] && state::remainingPower <= m_percentRemainingLevels[1])
	{
		// 30 < p <= 46		
		if(state::discharging)
		{
			// Discharging, NO red/ 1 Green
			TurnOn(3);
		}
		else
		{
			// Charging, Red off / Blink 2 Green
			Blink(2);
		}
	}
	else if(state::remainingPower > m_percentRemainingLevels[1] && state::remainingPower <= m_percentRemainingLevels[2])
	{
		// 46 < p <= 63		
		if(state::discharging)
		{
			// Discharging, NO red/ 2 Green
			TurnOn(2);
		}
		else
		{
			// Charging, Red off / Blink 3 Green
			Blink(1);
		}
	}
	else if(state::remainingPower > m_percentRemainingLevels[2] && state::remainingPower <= m_percentRemainingLevels[3])
	{
		// 63 < p <= 79	
		if(state::discharging)
		{
			// Discharging, NO red/ 3 Green
			TurnOn(1);
		}
		else
		{
			// Charging, Red off / Blink 4 Green
			Blink(0);
		}
	}
	else if(state::remainingPower > m_percentRemainingLevels[3])
	{
		// 79 < p <= 100	
		if(state::discharging)
		{
			// Discharging, NO red/ 4 Green
			TurnOn(0);
		}
		else
		{
			// Charging, Red off / Blink 4 Green
			TurnOn(0);
		}
	}
}

StatusLights::ERetcode StatusLights::ConfigureChip()
{
	// Reset values
	m_values = ~m_powerBit;

    auto ret = pca9538::SetConfig( m_i2c, m_values );
	if( ret )
	{
		// Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	ret = pca9538::SetOutput( m_i2c, 0x00 );
	if( ret )
	{
		// Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	return ERetcode::SUCCESS;
}

StatusLights::ERetcode StatusLights::SetPowerPattern()
{
	// Set the current values
	auto ret = pca9538::SetConfig( m_i2c, m_values );
	if( ret )
	{
		// Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	return ERetcode::SUCCESS;
}

#if 0
StatusLights::ERetcode StatusLights::ClearPowerPattern()
{
	// Set just the power bit
	auto ret = pca9538::SetConfig( mp_i2c, ~m_powerBit );
	if( ret )
	{
		// Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		m_i2c.UpdateFailures( true );
		return ERetcode::ERROR_IO;
	}

	return ERetcode::SUCCESS;
}
#endif

StatusLights::ERetcode StatusLights::CheckHealth()
{
	uint8_t output;
	auto ret = pca9538::ReadOutputs( m_i2c, output );
    if( ret )
	{   // Fail
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_IO );
		UpdateFailures( true );
		return ERetcode::ERROR_IO;
    }

	// If the output values are not 0, the chip is entered a bad state
	if( output != 0 )
	{
		m_subsystem.SetLastErrorCode( ERetcode::ERROR_BAD_STATE );
		UpdateFailures( false );
		return ERetcode::ERROR_BAD_STATE;
	}

	UpdateFailures( false );
	return ERetcode::SUCCESS;
}
