#include "orutil.h"
#include <xtimer.h>

namespace orutil
{
    // CTimer
    uint32_t CTimer::Now()
    {
        // Returns the current time in microseconds
        return ( xtimer_now() );
    }

    bool CTimer::HasElapsed( uint32_t usIn )
    {
        if( ( Now() - m_lastTime_us ) > usIn )
        {
            // Update the last time to the current time
            m_lastTime_us = Now();
            return true;
        }

        return false;
    }

    void CTimer::Reset()
    {
        m_lastTime_us = Now();
    }
}
