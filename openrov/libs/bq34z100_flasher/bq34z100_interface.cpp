#include "bq34z100_interface.h"

namespace bq34z100_interface
{
    // Methods
    i2c_retcode_t ITEnable(I2C* dev)
    {
        uint8_t itEnableBuffer[2] = {
            control_subcommands::IT_ENABLE_LSB,
            control_subcommands::IT_ENABLE_MSB
        };
        
        return dev->WriteRegisterBytes(shifted_register_address::DEVICE_ADDRESS, commands::CONTROL, itEnableBuffer, 2);
    }

    i2c_retcode_t Reset(I2C* dev, uint8_t slaveAddr)
    {
        uint8_t resetBuffer[2] = {
            control_subcommands::RESET_LSB,
            control_subcommands::RESET_MSB
        };

        return dev->WriteRegisterBytes(slaveAddr, commands::CONTROL, resetBuffer, 2);
    }
    
    i2c_retcode_t Seal(I2C* dev)
    {
        uint8_t sealBuffer[2] = {
            control_subcommands::SEALED_LSB,
            control_subcommands::SEALED_MSB
        };

        return dev->WriteRegisterBytes(shifted_register_address::DEVICE_ADDRESS, commands::CONTROL, sealBuffer, 2);
    }

    // Status methods
    i2c_retcode_t IsITEnabled(I2C* dev, bool &isITEnabledOut)
    {
        uint8_t data;
        auto ret = dev->ReadRegisterByte(shifted_register_address::DEVICE_ADDRESS, extended_commands::LEARNED_STATUS, &data );
        if( ret ){ return ret; }

        // Return state of 3rd bit, ITEN (Impedance tracking enabled)
        isITEnabledOut = ( data & (1 << 2) );
        return ret;        
    }

    i2c_retcode_t IsSealed(I2C* dev, bool &isSealedOut)
    {
        uint8_t data[2];
        auto ret = ReadControlSubcommandBytes(dev, control_subcommands::CONTROL_STATUS_LSB, data);
        if( ret ){ return ret; }

        // Return state of 6th bit, SS (sealed state)
        isSealedOut = ( data[1] & (1 << 5) );
        return ret;
    }

    i2c_retcode_t ReadAverageCurrent(I2C* dev, int16_t &currentOut_ma)
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes(dev, commands::AVERAGE_CURRENT, data);
        if( ret ){ return ret; }

        // Combine bytes
        currentOut_ma = static_cast<int16_t>( ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ] );
        return ret;
    }

    i2c_retcode_t ReadAveragePower(I2C* dev, uint32_t &powerOut_mw)
    {
        uint8_t data[2];
        auto ret = dev->ReadRegisterBytes(shifted_register_address::DEVICE_ADDRESS, extended_commands::AVERAGE_POWER, data, 2 );
        if( ret ){ return ret; }

        // Unit = 10mW, divide by 10
        powerOut_mw = ( ( (uint32_t)data[ 1 ] << 8 ) | data[ 0 ] ) / 10;
        return ret;
    }

    i2c_retcode_t ReadInstantCurrent(I2C* dev, int16_t &currentOut_ma)
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes(dev, commands::CURRENT, data);
        if( ret ){ return ret; }

        // Combine bytes
        currentOut_ma = static_cast<int16_t>( ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ] );
        return ret;
    }

    // TODO: configure  Cell Termination Voltage, this will read 0 until then
    i2c_retcode_t ReadRemainingCapacity(I2C* dev, uint16_t &capacityOut_mah)
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes(dev, commands::REMAINING_CAPACITY, data);
        if( ret ){ return ret; }

        capacityOut_mah = ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ];
        return ret;
    }

    i2c_retcode_t ReadTemperature(I2C* dev, uint32_t &temperatureOut_k)
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes(dev, commands::TEMPERATURE, data);
        if( ret ){ return ret; }

        // Unit = 0.1K, multiply by 10
        temperatureOut_k = ( ( (uint32_t)data[ 1 ] << 8 ) | data[ 0 ] ) * 10;
        return ret;
    }

    i2c_retcode_t ReadFlags(I2C* dev, uint16_t &flagsOut)
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes(dev, commands::FLAGS, data);
        if( ret ){ return ret; }

        flagsOut = ( (uint16_t)data[ 1 ] << 8 ) | data[ 0 ];
        return ret;
    }

    i2c_retcode_t ReadStateOfCharge(I2C* dev, uint8_t &percentOut)
    {
        uint8_t data[2];
        auto ret = ReadCommandBytes(dev, commands::STATE_OF_CHARGE, data);
        if( ret ){ return ret; }

        // Percent is LSB
        percentOut = data[ 0 ];
        return ret;
    }

    i2c_retcode_t ReadVoltage(I2C* dev, uint16_t& voltageMVOut)
    {
        uint8_t readBuffer[2] = {};

        auto retCode = dev->ReadRegisterBytes(shifted_register_address::DEVICE_ADDRESS, commands::VOLTAGE, readBuffer, 2);
        if(retCode)
        {
            return retCode;
        }

        // Combine the bytes into one integer for the reported voltage (mv)
        voltageMVOut = readBuffer[0] | readBuffer[1] << 8;
        return i2c_retcode_t::I2C_RETCODE_SUCCESS;
    }
    i2c_retcode_t ReadFirmwareVersion(I2C* dev, uint8_t* firmwareVersionOut)
    {
        uint8_t firmwareCommandBuffer[2] = {
            control_subcommands::FW_VERSION_LSB,
            control_subcommands::FW_VERSION_MSB
        };

        auto retCode = dev->WriteRegisterBytes(shifted_register_address::DEVICE_ADDRESS, commands::CONTROL, firmwareCommandBuffer, 2);
        if(retCode)
        {
            return retCode;
        }

        // Firmware version range: 0x0000 -> 0xFFFF (hex)
        retCode = dev->ReadRegisterBytes(shifted_register_address::DEVICE_ADDRESS, commands::CONTROL, firmwareVersionOut, 2);
        if(retCode)
        {
            return retCode;
        }

        return i2c_retcode_t::I2C_RETCODE_SUCCESS; 
    }

    i2c_retcode_t ReadFirmwareVersionROM(I2C* dev, uint8_t* firmwareVersionOut)
    {
        uint8_t firmwareCommandBuffer[2] = {
            control_subcommands::FW_VERSION_LSB,
            control_subcommands::FW_VERSION_MSB
        };

        auto retCode = dev->WriteRegisterBytes(shifted_register_address::ROM_MODE_ADDRESS, commands::CONTROL, firmwareCommandBuffer, 2);
        if(retCode)
        {
            return retCode;
        }

        // Firmware version range: 0x0000 -> 0xFFFF (hex)
        retCode = dev->ReadRegisterBytes(shifted_register_address::ROM_MODE_ADDRESS, commands::CONTROL, firmwareVersionOut, 2);
        if(retCode)
        {
            return retCode;
        }

        return i2c_retcode_t::I2C_RETCODE_SUCCESS; 
    }
}