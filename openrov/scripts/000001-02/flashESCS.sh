#!/bin/bash
set -e

if [ "$UID" != 0 ] ; then
  echo "$0: This script needs to be run as root" 1>&2
  exit 1
fi

firmware=$1
shift || true

if [ "$firmware" == "" ] ; then
  firmware=/opt/openrov/firmware/000001-02/pac5220/trident.bin
fi

esc=$1
shift || true

if [ "$esc" == "" ] ; then
  esc="all"
fi

result=0

# Flash ESCA

if [ "$esc" == "star" -o "$esc" == "all" ] ; then

    for ((attempt = 0; attempt < 3; attempt++)) ; do
        echo "=== Flashing ESCA / Starboard using $firmware ==="
    
        /opt/openrov/firmware/scripts/000001-02/selectESCA.sh
        sleep 0.2
        if openocd -f /usr/share/openocd/scripts/board/openrov_trident_pac.cfg -c "program $firmware verify; reset; exit"; then
            success=true
            break
        fi
  
        echo "*** Failed ***"
        success=false
        sleep 5
    done   

    if ! $success; then
        result=1    
    fi
    
    sleep 0.2
    
else
    echo "Not flashing ESCA / Starboard"
fi
    


# Flash ESCB

if [ "$esc" == "vert" -o "$esc" == "all" ] ; then
    
    for ((attempt = 0; attempt < 3; attempt++)) ; do        
        echo "=== Flashing ESCB / Vertical using $firmware ==="
        
        /opt/openrov/firmware/scripts/000001-02/selectESCB.sh
        sleep 0.2
        if openocd -f /usr/share/openocd/scripts/board/openrov_trident_pac.cfg -c "program $firmware verify; reset; exit"; then
            success=true
            break
        fi

        echo "*** Failed ***"
        success=false
        sleep 5
    done
  
    if ! $success; then
        result=$(($result + 2))
    fi
  
    sleep 0.2
    
else
    echo "Not flashing ESCB / Vertical"
fi
    

# Flash ESCC

if [ "$esc" == "port" -o "$esc" == "all" ] ; then
   
    for ((attempt = 0; attempt < 3; attempt++)) ; do
        echo "=== Flashing ESCC / Port using $firmware ==="
    
        /opt/openrov/firmware/scripts/000001-02/selectESCC.sh
        sleep 0.2
        if openocd -f /usr/share/openocd/scripts/board/openrov_trident_pac.cfg -c "program $firmware verify; reset; exit"; then
            success=true
            break
        fi

        echo "*** Failed ***"
        success=false
        sleep 5
    done
  
    if ! $success; then
        result=$(($result + 4))
    fi   
    
 else
    echo "Not flashing ESCC / Port"
fi
     


echo "$0 exit with status $result"

# Failure result is a bitset of A B C failures
exit $result


