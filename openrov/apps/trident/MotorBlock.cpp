#include "MotorBlock.h"
#include "NState.h"

#include "CommLink.h"

MotorBlock::MotorBlock()
	: m_portMotor( CommLink::GetChannel( CommLink::PORT_CHANNEL ), OROV_TRIDENT_MCU_ID_PORT_ESC, OROV_TRIDENT_SUBSYSTEM_ID_PORT_MOTOR, "PORT" )
	, m_vertMotor( CommLink::GetChannel( CommLink::VERT_CHANNEL ), OROV_TRIDENT_MCU_ID_VERT_ESC, OROV_TRIDENT_SUBSYSTEM_ID_VERT_MOTOR, "VERT" )
	, m_starMotor( CommLink::GetChannel( CommLink::STAR_CHANNEL ), OROV_TRIDENT_MCU_ID_STAR_ESC, OROV_TRIDENT_SUBSYSTEM_ID_STAR_MOTOR, "STAR" )
{
}

void MotorBlock::Initialize()
{
	// Reset Timer
	m_timeoutTimer.Reset();
	m_controlTimer.Reset();
    m_zeroTimer.Reset();

	m_portMotor.Initialize();
	m_vertMotor.Initialize();
	m_starMotor.Initialize();
}

void MotorBlock::Post()
{
	// Nothing to do. ESCs forward their post state
}

void MotorBlock::Update()
{
	// Handle incoming control setpoint messages from SAMD
	HandleMessages();

	// Handle updates for each ESC
	// ESC:
	// 	Incoming: Telemetry and ACKNACKS
	// 	Outgoing: Commands and param adjustments
	// SAMD:
	// 	Incoming: Commands and param adjustments
	//	Outgoing: Total passthrough + System/SubsystemStatus
	m_portMotor.Update();
	m_vertMotor.Update();
	m_starMotor.Update();

	// If we haven't received a motor command in 1 second, turn the motors off
	if( m_timeoutTimer.HasElapsed( 1000000 ) )
	{
		m_command.port	= 0.0f;
		m_command.vert	= 0.0f;
		m_command.star	= 0.0f;
	}
	
    // Don't sent zeros after 5 seconds
	
	if (m_command.port == 0.0 && m_command.vert == 0.0 && m_command.star == 0.0) {
        if (m_zeroTimer.HasElapsed( 5 * 1000 * 1000)) {
            return;
        }        
    } else {
        m_zeroTimer.Reset();
    }


	if( m_controlTimer.HasElapsed( 30000 )  )
	{
		// Set target speeds
		m_portMotor.SendSpeedTarget( m_command.port );
		m_vertMotor.SendSpeedTarget( m_command.vert );
		m_starMotor.SendSpeedTarget( m_command.star );
	}
}

void MotorBlock::HandleMessages()
{
	int messageId = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessageID();
    
    switch( messageId )
    {
		case MAVLINK_MSG_ID_OROV_TRIDENT_MOTOR_COMMAND:
        {
            mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();
            
			// Get port, vert, and star target powers
			m_command.sequence 	= mavlink_msg_orov_trident_motor_command_get_sequence( message );
			m_command.port 		= mavlink_msg_orov_trident_motor_command_get_port( message );
			m_command.vert 		= mavlink_msg_orov_trident_motor_command_get_vert( message );
			m_command.star 		= mavlink_msg_orov_trident_motor_command_get_star( message );

			// Reset timeout counter
			m_timeoutTimer.Reset();

			// mavlink_msg_orov_trident_motor_command_send_struct( CommLink::MAIN_CHANNEL, &m_command );
			break;
		}
		
        case MAVLINK_MSG_ID_OROV_TRIDENT_MOTOR_CONFIG_COMMAND:
        {
            mavlink_message_t* message  = CommLink::GetChannel( CommLink::MAIN_CHANNEL )->GetCurrentMessage();
            
            auto id      = mavlink_msg_orov_trident_motor_config_command_get_id( message );
            auto min     = mavlink_msg_orov_trident_motor_config_command_get_min( message );
            auto max     = mavlink_msg_orov_trident_motor_config_command_get_max( message );
            auto timeout = mavlink_msg_orov_trident_motor_config_command_get_timeout( message );
            
            if (id ==  OROV_TRIDENT_MCU_ID_PORT_ESC) {
                m_portMotor.SendMinMaxError(min, max, timeout); 
            } else if (id == OROV_TRIDENT_MCU_ID_STAR_ESC) {
                m_starMotor.SendMinMaxError(min, max, timeout); 
            } else if (id == OROV_TRIDENT_MCU_ID_VERT_ESC) {
                m_vertMotor.SendMinMaxError(min, max, timeout); 
            }
            
            break;
        }

		default:
		{            
			break;
		}
	}
}
