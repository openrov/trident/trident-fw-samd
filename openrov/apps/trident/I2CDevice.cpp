
#include "I2CDevice.h"
#include "I2CMonitor.h"


I2CDevice::I2CDevice( EI2CDeviceId idIn, OROV_TRIDENT_SUBSYSTEM_ID systemId, 
               const char* nameIn, uint32_t failureLimitIn )
        : m_subsystem( systemId, nameIn )
        , m_i2c{ &I2CMonitor::I2CMain }        
        , m_id{ idIn }
        , m_name{ nameIn }
        , m_failureLimit{ failureLimitIn }
{
}


void I2CDevice::I2CEventCallback( EI2CEvent eventIn )
{
    switch( eventIn )
    {
        case EI2CEvent::POWER_RESET:
        {
            // Move device to reset substate
            m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_RECOVERY );
            m_subsystem.TransitionSubstate( EReservedSubstate::RESET );
            break;
        }

        case EI2CEvent::BUS_UNAVAILABLE:
        {
            // If not already in standby
            if( m_subsystem.GetState() != OROV_SUBSYSTEM_STATE_STANDBY )
            {
                // Move device into standby
                m_subsystem.StoreStateAndSubstate();
                m_subsystem.TransitionState( OROV_SUBSYSTEM_STATE_STANDBY );
            }

            break;
        }

        case EI2CEvent::BUS_AVAILABLE:
        {
            // If in standby
            if( m_subsystem.GetState() == OROV_SUBSYSTEM_STATE_STANDBY )
            {
                // Restore the original state
                m_subsystem.RestoreStateAndSubstate();
            }

            break;
        }

        default:
        {
            // Unsupported I2C event
            break;
        }
    }
}

