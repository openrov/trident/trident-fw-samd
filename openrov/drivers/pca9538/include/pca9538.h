#pragma once

#include <i2cplus.h>
#include <log.h>

namespace pca9538
{
    // I2C Slave Address
    namespace address
    {
        constexpr uint8_t A = 0x70;
    }

    // Computation constants
    namespace constant
    {
        constexpr uint8_t kInputRegister    = 0x00;
        constexpr uint8_t kOutputRegister   = 0x01;
	    constexpr uint8_t kConfigRegister   = 0x03;
    }

    // Methods
    // 1s are inputs, 0s are outputs
    inline i2c_retcode_t SetConfig( I2C* dev, uint8_t configIn )
    {
        return dev->WriteRegisterByte( address::A, constant::kConfigRegister, configIn );
    }

    inline i2c_retcode_t SetOutput( I2C* dev, uint8_t valuesIn )
    {
        return dev->WriteRegisterByte( address::A, constant::kOutputRegister, valuesIn );
    }

    inline i2c_retcode_t ReadConfig( I2C* dev, uint8_t &valuesOut )
    {
        return dev->ReadRegisterByte( address::A, constant::kConfigRegister, &valuesOut );
    }

    inline i2c_retcode_t ReadInputs( I2C* dev, uint8_t &valuesOut )
    {
        return dev->ReadRegisterByte( address::A, constant::kInputRegister, &valuesOut );
    }

    inline i2c_retcode_t ReadOutputs( I2C* dev, uint8_t &valuesOut )
    {
        return dev->ReadRegisterByte( address::A, constant::kOutputRegister, &valuesOut );
    }
}