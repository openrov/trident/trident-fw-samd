 /*
 *  Notes:
 *  	Examples taken from SparkFun Arduino BNO080 library
 *  		https://github.com/sparkfun/SparkFun_BNO080_Arduino_Library
 */
#include <bno080.h>
#include "xtimer.h"


extern void sendDebug(uint32_t time, uint8_t ind, float value);

namespace bno080{
	BNO080::BNO080(I2C* _dev, uint8_t _slaveAddr) 
    : BNO(_dev, _slaveAddr)
    // Current bno080 mount is 180 degrees around z. i.e. x is pointing to the stern
    , m_quat_z_180rot(bno080::Quaternion( 0, 0, 0, 1 ))
	{
		//Set new message flags to false which are updated upon successful decode of new measurement
		m_newAngVel = false;
		m_newQuat 	= false;
	}

	BNO080::~BNO080()
	{
	}

	i2c_retcode_t BNO080::Reset( void )
	{
        i2c_retcode_t ret;
        
        uint8_t packetLength = 1;

        memset( shtpData, 0xFF, sizeof(shtpData)/*bno080::constants::MAX_PACKET_SIZE*/ );

        shtpData[0] = 1;

        ret = SendPacket( bno080::channels::CHANNEL_EXECUTABLE, packetLength );
        
        m_newAngVel = false;
        m_newQuat 	= false;
                
        memset(sequenceNumber, 0, sizeof(sequenceNumber));
        
        // Reset response
        // 1 = POR, 2 = Internal reset, 3 = Watchdog, 4 = External reset, 5 = Other
        xtimer_usleep(bno080::constants::MODE_SWITCH_DELAY_US);
        ret = dev->ReadBytes( slaveAddr, shtpData, bno080::constants::I2C_BUFFER_LENGTH );
        /*
        for( unsigned int i = 0; i < bno080::constants::I2C_BUFFER_LENGTH; ++i)
        {
        	sendDebug(200, i, shtpData[i]);
        }
        */
        return ret;
	}

	i2c_retcode_t BNO080::SendPacket( uint8_t channel, uint8_t packetLength )
	{
		memset( shtpPacket, 0xFF, sizeof(shtpPacket) );

		if( dev->Available() )
		{
			shtpPacket[0] = (packetLength & 0xFF); 				// Packet length LSB
			shtpPacket[1] = (packetLength >> 8 );				// Packet length MSB
			shtpPacket[2] = channel;							// Channel
			shtpPacket[3] = sequenceNumber[channel]++;

			uint8_t dataLength = packetLength + 4; // add header
			for( uint8_t byte = 4; byte < dataLength; byte++ )
			{
				shtpPacket[byte] = shtpData[byte - 4];
			}
			
			return dev->WriteBytes( slaveAddr, shtpPacket, /*dataLength */ sizeof(shtpPacket) );
		}
		return I2C_RETCODE_ERROR_BAD_DEV;
	}

	i2c_retcode_t BNO080::SendCalPacket( uint8_t channel, uint8_t packetLength )
	{
		memset( shtpPacket, 0xFF, sizeof(shtpPacket) );

		if( dev->Available() )
		{
			shtpPacket[0] = (packetLength & 0xFF); 				// Packet length LSB
			shtpPacket[1] = (packetLength >> 8 );				// Packet length MSB
			shtpPacket[2] = channel;							// Channel

			uint8_t dataLength = packetLength + 3; // add header
			for( uint8_t byte = 4; byte < dataLength; byte++ )
			{
				shtpPacket[byte] = shtpData[byte - 3];
			}
 			return dev->WriteBytes( slaveAddr, shtpPacket, /*dataLength */  sizeof(shtpPacket) );
		}
		return I2C_RETCODE_ERROR_BAD_DEV;
	}
	
	i2c_retcode_t BNO080::Configure( ) 
    {
        // Enable Gyro
        auto ret = EnableGyro();
        if( ret )
        {
            //Fail
            return ret;
        }

        xtimer_usleep(bno080::constants::MODE_SWITCH_DELAY_US);

        // Enable Accel
        ret = EnableAccel();
        if( ret )
        {
            //Fail
            return ret;
        }

        xtimer_usleep(bno080::constants::MODE_SWITCH_DELAY_US);

        // Enable Accel
        ret = EnableMagnetometer();
        if( ret )
        {
            //Fail
            return ret;
        }

        xtimer_usleep(bno080::constants::MODE_SWITCH_DELAY_US);

        // Enable Fusion
	   ret = EnableRotationVector();
	   if( ret )
	   {
		   //Fail
		   return ret;
	   }
//       sendDebug(500, 1, 5000+ret);
	   xtimer_usleep(bno080::constants::MODE_SWITCH_DELAY_US);
        
        return ret;
    }

	i2c_retcode_t BNO080::VerifyChipId( bool &validChip )
	{
		(void)validChip;
		return I2C_RETCODE_SUCCESS;
	}

    i2c_retcode_t BNO080::VerifyChipIdGet( uint8_t *validChip )
	{
        memset(shtpData, 0xFF, sizeof(shtpData));
        
		shtpData[0] = bno080::shtp::SHTP_REPORT_PRODUCT_ID_REQUEST; //Request the product ID and reset info
		shtpData[1] = 0; //Reserved
		
		memset(validChip, 0xff, 32);

		uint8_t packetLength = 2; // 2 bytes

		auto ret = SendPacket( bno080::channels::CHANNEL_CONTROL, packetLength  /*bno080::constants::I2C_BUFFER_LENGTH*/);
		if( ret )
		{
			// fail
			return ret;
		}

		xtimer_usleep(bno080::constants::MODE_SWITCH_DELAY_US);
		ret = dev->ReadBytes( slaveAddr, shtpData, bno080::constants::I2C_BUFFER_LENGTH );
        

//		for( unsigned int i = 0; i < bno080::constants::I2C_BUFFER_LENGTH; ++i)
//		{
//			sendDebug(300, i, shtpData[i]);
//		}

		if( ret )
		{
			// fail
			return ret;
                        
 		}

 		memcpy(validChip, shtpData, sizeof(validChip));
                    
		return I2C_RETCODE_SUCCESS;
	}
	
	i2c_retcode_t BNO080::EnableAccel( void )
	{
		uint8_t packetLength = 17;

		SetFeatureCommand( bno080::reportIds::SENSOR_REPORTID_ACCELEROMETER, 50, 0 ); // 50ms

		return SendPacket( bno080::channels::CHANNEL_CONTROL, packetLength );
	}

	i2c_retcode_t BNO080::EnableRotationVector( void )
	{
		uint8_t packetLength = 17;

		SetFeatureCommand( bno080::reportIds::SENSOR_REPORTID_ROTATION_VECTOR, 50, 0 ); // 50ms

		return SendPacket( bno080::channels::CHANNEL_CONTROL, packetLength );
	}

	i2c_retcode_t BNO080::EnableGyro( void )
	{
		uint8_t packetLength = 17;

		SetFeatureCommand( bno080::reportIds::SENSOR_REPORTID_GYROSCOPE, 50, 0 ); // 50ms

		return SendPacket( bno080::channels::CHANNEL_CONTROL, packetLength );
	}

	i2c_retcode_t BNO080::EnableMagnetometer( void )
	{
		uint8_t packetLength = 17;

		SetFeatureCommand( bno080::reportIds::SENSOR_REPORTID_MAGNETIC_FIELD, 50, 0 ); // 10ms

		return SendPacket( bno080::channels::CHANNEL_CONTROL, packetLength );
	}

	i2c_retcode_t BNO080::Calibrate( void )
	{
		uint8_t packetLength = 12;

		/*	shtpData[3] = 0; //P0 - Accel Cal Enable
			shtpData[4] = 0; //P1 - Gyro Cal Enable
			shtpData[5] = 0; //P2 - Mag Cal Enable
			shtpData[6] = 0; //P3 - Subcommand 0x00
			shtpData[7] = 0; //P4 - Planar Accel Cal Enable
		*/

		// Clear shtpdata for calibration
		for( uint8_t i = 3; i < 8; ++i )
		{
			shtpData[i] = 0;
		}

		// Calibrate all component sensors
		shtpData[3] = 1;
		shtpData[4] = 1;
		shtpData[5] = 1;

		// Set status to failed ( 0 = success ) until clear
		m_calibrationStatus = 1;

		return SendCalPacket( bno080::commandIds::COMMAND_ME_CALIBRATE, packetLength );

		// TODO - Implement request status for calibration feedback
	}
//	calibration support methods
//	//See 2.2 of the Calibration Procedure document 1000-4044
//	void BNO080::calibrateAll()
//	{
//		sendCalibrateCommand(CALIBRATE_ACCEL_GYRO_MAG);
//	}
//
//	void BNO080::endCalibration()
//	{
//		sendCalibrateCommand(CALIBRATE_STOP); //Disables all calibrations
//	}
//
//	//See page 51 of reference manual - ME Calibration Response
//	//Byte 5 is parsed during the readPacket and stored in calibrationStatus
//	boolean BNO080::calibrationComplete()
//	{
//		if (calibrationStatus == 0)
//			return (true);
//		return (false);
//	}
//	//Request ME Calibration Status from BNO080
//	//See page 51 of reference manual
//	void BNO080::requestCalibrationStatus()
//	{
//		/*shtpData[3] = 0; //P0 - Reserved
//		shtpData[4] = 0; //P1 - Reserved
//		shtpData[5] = 0; //P2 - Reserved
//		shtpData[6] = 0; //P3 - 0x01 - Subcommand: Get ME Calibration
//		shtpData[7] = 0; //P4 - Reserved
//		shtpData[8] = 0; //P5 - Reserved
//		shtpData[9] = 0; //P6 - Reserved
//		shtpData[10] = 0; //P7 - Reserved
//		shtpData[11] = 0; //P8 - Reserved*/
//
//		for (uint8_t x = 3; x < 12; x++) //Clear this section of the shtpData array
//			shtpData[x] = 0;
//
//		shtpData[6] = 0x01; //P3 - 0x01 - Subcommand: Get ME Calibration
//
//		//Using this shtpData packet, send a command
//		sendCommand(COMMAND_ME_CALIBRATE);
//	}
//
//	//This tells the BNO080 to save the Dynamic Calibration Data (DCD) to flash
//	//See page 49 of reference manual and the 1000-4044 calibration doc
//	void BNO080::saveCalibration()
//	{
//		/*shtpData[3] = 0; //P0 - Reserved
//		shtpData[4] = 0; //P1 - Reserved
//		shtpData[5] = 0; //P2 - Reserved
//		shtpData[6] = 0; //P3 - Reserved
//		shtpData[7] = 0; //P4 - Reserved
//		shtpData[8] = 0; //P5 - Reserved
//		shtpData[9] = 0; //P6 - Reserved
//		shtpData[10] = 0; //P7 - Reserved
//		shtpData[11] = 0; //P8 - Reserved*/
//
//		for (uint8_t x = 3; x < 12; x++) //Clear this section of the shtpData array
//			shtpData[x] = 0;
//
//		//Using this shtpData packet, send a command
//		sendCommand(COMMAND_DCD); //Save DCD command
//	}

	void BNO080::SetFeatureCommand( uint8_t reportID, uint16_t timeBetweenReports, uint32_t specificConfig )
	{
		long microsBetweenReports = (long)timeBetweenReports * 1000L;

		shtpData[0] 	= bno080::shtp::SHTP_REPORT_SET_FEATURE_COMMAND;//Set feature command. Reference page 55
		shtpData[1] 	= reportID; 									//Feature Report ID. 0x01 = Accelerometer, 0x05 = Rotation vector
		shtpData[2] 	= 0; 											//Feature flags
		shtpData[3] 	= 0; 											//Change sensitivity (LSB)
		shtpData[4] 	= 0; 											//Change sensitivity (MSB)
		shtpData[5] 	= (microsBetweenReports >> 0) & 0xFF; 			//Report interval (LSB) in microseconds. 0x7A120 = 500ms
		shtpData[6] 	= (microsBetweenReports >> 8) & 0xFF; 			//Report interval
		shtpData[7] 	= (microsBetweenReports >> 16) & 0xFF; 			//Report interval
		shtpData[8] 	= (microsBetweenReports >> 24) & 0xFF; 			//Report interval (MSB)
		shtpData[9] 	= 0; 											//Batch Interval (LSB)
		shtpData[10] 	= 0; 											//Batch Interval
		shtpData[11] 	= 0; 											//Batch Interval
		shtpData[12] 	= 0; 											//Batch Interval (MSB)
		shtpData[13] 	= (specificConfig >> 0) & 0xFF; 				//Sensor-specific config (LSB)
		shtpData[14] 	= (specificConfig >> 8) & 0xFF; 				//Sensor-specific config
		shtpData[15] 	= (specificConfig >> 16) & 0xFF; 				//Sensor-specific config
		shtpData[16] 	= (specificConfig >> 24) & 0xFF; 				//Sensor-specific config (MSB)
	}
	
	/**
     * Note the disabled sanity checks in this method.  As it is, we're
     * overrunning the buffer (fortunately into next buffer we'd use any
     * way.  Enabling all these checks allows through some occasional bogus/zero
     * values. It's unclear why.
     */	

	i2c_retcode_t BNO080::ReadPackets( void )
	{
		// Get the first four bytes, aka the packet header

        /*
         * TODO: Strictly speaking, this is/was a buffer overrun:
         * 
         *  uint8_t 				shtpHeader[bno080::constants::HEADER_LEN];
         *  uint8_t					shtpThrowAway[4];
		 *  uint8_t 				shtpData[bno080::constants::MAX_PACKET_SIZE];
		 *  uint8_t 				shtpPacket[bno080::constants::MAX_PACKET_SIZE];
         * 
         * Although the bytes safely end up in unused following bytes.  We should just be
         * reading the header length, but this ends up causing bogus values for yaw, roll, pitch later 
         * on some reads.
         */ 
		auto ret = dev->ReadBytes(slaveAddr, shtpHeader, /*bno080::constants::HEADER_LEN*/ bno080::constants::I2C_BUFFER_LENGTH );

		if( ret )
		{
			// Fail
//			sendDebug(999, 1, 999);
			return ret;
		}

		//Store the header info.
		uint8_t packetLSB = shtpHeader[0];
		uint8_t packetMSB = shtpHeader[1];

		//Calculate the number of data bytes in this packet
		int16_t dataLength = ((uint16_t)packetMSB << 8 | packetLSB);

        //sendDebug(5, 0, dataLength);
        
		dataLength &= ~(1 << 15); //Clear the MSbit.
		//This bit indicates if this packet is a continuation of the last. Ignore it for now.

        // Includes length of header
		//if( dataLength < 4 )
        if( dataLength == 0 )
		{
			// Empty
//        	sendDebug(999, 1, 999);
			return I2C_RETCODE_ERROR_BAD_DEV;
		}
		
		// Ensure we have a valid command/channel
		/*if (shtpHeader[2] > bno080::channels::CHANNEL_GYRO) {
            return I2C_RETCODE_ERROR_BAD_DEV;
        }*/

//		return StoreData( dataLength);

        uint16_t dataSpot = 0;

		// Doing this in 32 byte chunks
		while( dataLength > 0)
		{
			uint16_t numberOfBytes = dataLength;


			if( numberOfBytes > bno080::constants::I2C_BUFFER_LENGTH )
			{
				numberOfBytes = bno080::constants::I2C_BUFFER_LENGTH;
			}

			// Get packet
			ret = dev->ReadBytes( slaveAddr, shtpPacket, numberOfBytes + bno080::constants::HEADER_LEN );

			if( ret )
			{
				// Fail
//				sendDebug(999, 1, 999);
				return ret;
			}

			for ( uint8_t i = bno080::constants::HEADER_LEN; i < numberOfBytes + bno080::constants::HEADER_LEN; ++i )
			{
				if( dataSpot < bno080::constants::MAX_PACKET_SIZE )
				{
					shtpData[dataSpot++] = shtpPacket[i];
				}
			}

			dataLength -= numberOfBytes;
		}

		return I2C_RETCODE_SUCCESS;
	}

	bool BNO080::DataAvailable( void )
	{
		auto ret = ReadPackets();

		if( ret )
		{
			// Fail
//			sendDebug(999, 1, 9991);
			return false;
		}

		if ( shtpHeader[2] == bno080::channels::CHANNEL_REPORTS && shtpData[0] == bno080::shtp::SHTP_REPORT_BASE_TIMESTAMP )
		{
			return ParseData();
            
		} else if( shtpData[2] == bno080::channels::CHANNEL_CONTROL )
		{
			// Update response to commands
			return ParseCommandReport();
		}

		return false;
	}

	bool BNO080::ParseCommandReport()
	{
		if ( shtpData[0] == bno080::shtp::SHTP_REPORT_COMMAND_RESPONSE )
		{
			//The BNO080 responds with this report to command requests. It's up to us to remember which command we issued.
			uint8_t command = shtpData[2]; //This is the Command byte of the response

			if ( command == bno080::commandIds::COMMAND_ME_CALIBRATE )
			{
				m_calibrationStatus = shtpData[5 + 0]; //R0 - Status (0 = success, non-zero = fail)
				return true;
			}
		}
		
		return true; // Could return false to indiicate non-parsing
	}

	/**
	* This function pulls the data from the input report
	*	The input reports vary in length so this function stores the various 16-bit values as globals

	*	Unit responds with packet that contains the following:
	*	shtpHeader[0:3]: First, a 4 byte header
	*	shtpData[0:4]: Then a 5 byte timestamp of microsecond clicks since reading was taken
	*	shtpData[5 + 0]: Then a feature report ID (0x01 for Accel, 0x05 for Rotation Vector)
	*	shtpData[5 + 1]: Sequence number (See 6.5.18.2)
	*	shtpData[5 + 2]: Status
	*	shtpData[3]: Delay
	*	shtpData[4:5]: i/accel x/gyro x/etc
	*	shtpData[6:7]: j/accel y/gyro y/etc
	*	shtpData[8:9]: k/accel z/gyro z/etc
	*	shtpData[10:11]: real/gyro temp/etc
	*	shtpData[12:13]: Accuracy estimate
	* **/
	bool BNO080::ParseData(void)
	{
		//Calculate the number of data bytes in this packet
		int16_t dataLength = ( (uint16_t)shtpHeader[1] << 8 | shtpHeader[0] );

		dataLength &= ~(1 << 15); //Clear the MSbit. This bit indicates if this package is a continuation of the last.
		//Ignore it for now. TODO catch this as an error and exit

        if (dataLength < 4 || dataLength > 32)
		{
            return false;
		}

		dataLength -= 4; //Remove the header bytes from the data count

		// added from Sparkfun update - TODO - impl GetTimeStamp()
		m_timeStamp = ((uint32_t)shtpData[4] << (8 * 3)) | ((uint32_t)shtpData[3] << (8 * 2)) | ((uint32_t)shtpData[2] << (8 * 1)) | ((uint32_t)shtpData[1] << (8 * 0));


		uint8_t status = shtpData[5 + 2] & 0x03; //Get status bits
		uint16_t data1 = (uint16_t)shtpData[5 + 5] << 8 | shtpData[5 + 4];
		uint16_t data2 = (uint16_t)shtpData[5 + 7] << 8 | shtpData[5 + 6];

		uint16_t data3 = (uint16_t)shtpData[5 + 9] << 8 | shtpData[5 + 8];
		uint16_t data4 = 0;
		uint16_t data5 = 0;

		if( dataLength - 5 > 9 )
		{
			data4 = (uint16_t)shtpData[5 + 11] << 8 | shtpData[5 + 10];
		}
		if( dataLength - 5 > 11 )
		{
			data5 = (uint16_t)shtpData[5 + 13] << 8 | shtpData[5 + 12];
		}

		//Store these generic values to their proper global variable
		if( shtpData[5] == bno080::reportIds::SENSOR_REPORTID_ACCELEROMETER )
		{
			m_accelAccuracy 	= status;
			m_rawAccelX 		= data1;
			m_rawAccelY 		= data2;
			m_rawAccelZ 		= data3;
		}
		else if( shtpData[5] == bno080::reportIds::SENSOR_REPORTID_GYROSCOPE )
		{
			m_gyroAccuracy 		= status;
			m_rawGyroX			= data1;
			m_rawGyroY 			= data2;
			m_rawGyroZ 			= data3;

			SetNewAngVelMsgAvailable( true );
		}
	   else if( shtpData[5] == bno080::reportIds::SENSOR_REPORTID_MAGNETIC_FIELD )
	   {
		   m_magAccuracy 		= status;
		   m_rawMagX 			= data1;
		   m_rawMagY 			= data2;
		   m_rawMagZ 			= data3;
	   }
	   else if( shtpData[5] == bno080::reportIds::SENSOR_REPORTID_ROTATION_VECTOR )
	   {
		   m_quatAccuracy 		= status;
		   m_rawQuatI 			= data1;
		   m_rawQuatJ 			= data2;
		   m_rawQuatK 			= data3;
		   m_rawQuatReal 		= data4;
		   m_rawQuatRadianAccuracy = data5; //Only available on rotation vector, not game rot vector

		   SetNewQuatMsgAvailable( true );
	   }
	   else if( shtpData[5] == bno080::shtp::SHTP_REPORT_COMMAND_RESPONSE )
	   {
		   //The BNO080 responds with this report to command requests. It's up to us to remember which command we issued.
		   uint8_t command = shtpData[5 + 2]; //This is the Command byte of the response

		   if( command == bno080::commandIds::COMMAND_ME_CALIBRATE )
		   {
			   m_calibrationStatus = shtpData[5 + 5]; //R0 - Status (0 = success, non-zero = fail)
		   }
	   }

	   return true;
	}

	// Getters
	void BNO080::SetNewQuatMsgAvailable( bool newQuatMsg )
	{
		m_newQuat = newQuatMsg;
	}

	void BNO080::SetNewAngVelMsgAvailable( bool newAngVelMsg )
	{
		m_newAngVel = newAngVelMsg;
	}

	bool BNO080::GetNewQuatMsgAvailable()
	{
		return m_newQuat;
	}

	bool BNO080::GetNewAngVelMsgAvailable()
	{
		return m_newAngVel;
	}

	float BNO080::GetGyroX()
	{
		float data = QToFloat( m_rawGyroX, m_gyro_Q1 );
		return data;
	}

	float BNO080::GetGyroY()
	{
		float data = QToFloat( m_rawGyroY, m_gyro_Q1 );
		return data;
	}

	float BNO080::GetGyroZ()
	{
		float data = QToFloat( m_rawGyroZ, m_gyro_Q1 );
		return data;
	}

	//Given a register value and a Q point, convert to float
	//See https://en.wikipedia.org/wiki/Q_(number_format)
	float BNO080::QToFloat( int16_t fixedPointValue, uint8_t qPoint )
	{
		float qFloat = fixedPointValue;
		qFloat *= pow(2, qPoint * -1);

		return ( qFloat );
	}

	// Return Accel X component
	float BNO080::GetAccelX()
	{
	  float accel = QToFloat( m_rawAccelX, m_accelerometer_Q1 );
	  return ( accel );
	}

	//Return the acceleration component
	float BNO080::GetAccelY()
	{
	  float accel = QToFloat( m_rawAccelY, m_accelerometer_Q1 );
	  return ( accel );
	}

	//Return the acceleration component
	float BNO080::GetAccelZ()
	{
	  float accel = QToFloat( m_rawAccelZ, m_accelerometer_Q1 );
	  return ( accel );
	}

	//Return the rotation vector quaternion I
	float BNO080::GetQuatI()
	{
	  float quat = QToFloat( m_rawQuatI, m_rotationVector_Q1 );
	  return ( quat );
	}

	//Return the rotation vector quaternion J
	float BNO080::GetQuatJ()
	{
	  float quat = QToFloat( m_rawQuatJ, m_rotationVector_Q1 );
	  return ( quat );
	}

	//Return the rotation vector quaternion K
	float BNO080::GetQuatK()
	{
	  float quat = QToFloat( m_rawQuatK, m_rotationVector_Q1);
	  return ( quat );
	}

	//Return the rotation vector quaternion Real
	float BNO080::GetQuatReal()
	{
	  float quat = QToFloat( m_rawQuatReal, m_rotationVector_Q1);
	  return ( quat );
	}

	//Return the rotation vector accuracy
	float BNO080::GetQuatRadianAccuracy()
	{
	  float quat = QToFloat( m_rawQuatRadianAccuracy, m_rotationVector_Q1);
	  return ( quat );
	}

	//Return the acceleration component
	uint8_t BNO080::GetQuatAccuracy()
	{
	  return ( m_quatAccuracy );
	}

	//Return the acceleration component
	uint8_t BNO080::GetAccelAccuracy()
	{
	  return ( m_accelAccuracy );
	}

	//Return the magnetometer component
	float BNO080::GetMagX()
	{
	  float mag = QToFloat( m_rawMagX, m_magnetometer_Q1 );
	  return ( mag );
	}

	//Return the magnetometer component
	float BNO080::GetMagY()
	{
	  float mag = QToFloat( m_rawMagY, m_magnetometer_Q1 );
	  return ( mag );
	}

	//Return the magnetometer component
	float BNO080::GetMagZ()
	{
	  float mag = QToFloat( m_rawMagZ, m_magnetometer_Q1 );
	  return ( mag );
	}

	//Return the mag component
	uint8_t BNO080::GetMagAccuracy()
	{
	  return ( m_magAccuracy );
	}

	// Abstract implementations
	i2c_retcode_t BNO080::ReadQuaternion( BNOQuaternion &quatOut )
    {
		(void)quatOut;
		return I2C_RETCODE_SUCCESS;
	}

    i2c_retcode_t BNO080::ReadGyro( TRawIMUData &dataOut )
    {
        (void)dataOut;
        return I2C_RETCODE_SUCCESS;
    }
}
