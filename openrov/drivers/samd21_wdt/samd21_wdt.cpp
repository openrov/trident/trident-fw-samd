#include "samd21_wdt.h"

#include <cpu.h>
#include <board.h>
#include <log.h>
#include <thread.h>

// Static initialization
volatile bool WatchdogTimer::earlyWarningTrigger = false;
volatile int WatchdogTimer::earlyWarningCount = 0;

// Setup ISR
extern "C"
{
    void isr_wdt(void) 
    {
        // Clear interrupt flag
        WDT->INTFLAG.bit.EW  = 1;        

        // Update flags and stats
        WatchdogTimer::earlyWarningTrigger = true;
        WatchdogTimer::earlyWarningCount++;

        if (sched_context_switch_request) {
            thread_yield_higher();
        }
    }
}

WatchdogTimer::WatchdogTimer()
    : m_initialized(false)
{
}

int WatchdogTimer::Enable( uint8_t timeoutPeriod, uint8_t earlyWarningPeriod ) 
{
    if( !m_initialized ) 
    {
        Initialize();
    }

    // Disable watchdog for config
    WDT->CTRL.reg = 0; 
    while(WDT->STATUS.bit.SYNCBUSY);

    // Make sure early warning is less than timeout period
    if( earlyWarningPeriod >= timeoutPeriod )
    {
        earlyWarningPeriod = ( timeoutPeriod - 1 );
    }

    // Make sure values are valid
    if( timeoutPeriod >= 0xC || earlyWarningPeriod >= 0xC )
    {
        return -1;
    }

    WDT->INTENSET.bit.EW        = 1;                    // Enable early warning interrupt
    WDT->CONFIG.bit.PER         = timeoutPeriod;        // Set period for chip reset
    WDT->EWCTRL.bit.EWOFFSET    = earlyWarningPeriod;   // Set period for chip reset
    WDT->CTRL.bit.WEN           = 0;                    // Disable window mode

    while(WDT->STATUS.bit.SYNCBUSY);

    // Clear watchdog interval
    Reset();                

    // Start watchdog now!  
    WDT->CTRL.bit.ENABLE = 1; 
    while(WDT->STATUS.bit.SYNCBUSY);

    return 0;
}

void WatchdogTimer::Reset() 
{
    // Write the watchdog clear key value (0xA5) to the watchdog
    // clear register to clear the watchdog timer and reset it.
    while( WDT->STATUS.bit.SYNCBUSY );
    WDT->CLEAR.reg = WDT_CLEAR_CLEAR_KEY;
}

void WatchdogTimer::Disable() 
{
    WDT->CTRL.bit.ENABLE = 0;
    while(WDT->STATUS.bit.SYNCBUSY);
}

void WatchdogTimer::Initialize() 
{
    LOG_DEBUG( "Initializing WDT clock\n" );

    // Turn on power for WDT
    PM->APBAMASK.reg |= PM_APBAMASK_WDT;

    // Attach WDT to GCLK2
    GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID_WDT |
                        GCLK_CLKCTRL_CLKEN |
                        GCLK_CLKCTRL_GEN_GCLK2;

    while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) {}

    // Enable WDT early-warning interrupt
    NVIC_DisableIRQ(WDT_IRQn);
    NVIC_ClearPendingIRQ(WDT_IRQn);
    NVIC_SetPriority(WDT_IRQn, 0); // Top priority
    NVIC_EnableIRQ(WDT_IRQn);

    m_initialized = true;
}