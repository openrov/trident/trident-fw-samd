#!/bin/bash
set -e

# Boards to build for
BOARD_LIST=(000001-02)

# Apps to build
# DANGER: Do not use UTF-8 name for these directories. The build system/firmware is too fragile to handle that right now.
# TODO: Fix the above ^

apps=$1

if [ "$apps" != "" ] ; then
    APP_LIST=($apps)
else
    APP_LIST=(empty fuelgauge trident)
fi


# Source directory
SOURCE_DIR=${PWD}

# Build directory
mkdir -p build
BUILD_DIR=${PWD}/build/opt/openrov/firmware

# Get the githash
GIT_HASH_SHORT=$(git rev-parse --short=7 HEAD)
GIT_DESCRIBE=$(git describe --long --all)
CPU_NAME="samd21"

# Loop through each version in the types file and generate the 
for board in "${BOARD_LIST[@]}"
do
    for app in "${APP_LIST[@]}"
    do
        pushd ${SOURCE_DIR}/openrov/apps/${app}
        
        # Get the short App ID by taking up to the first 5 characters from the app name (format: ${var:start:length})
        APP_ID_SHORT=${app:0:5}

        # Build
        make -j BOARD=${board} CFG=release GIT_HASH=${GIT_HASH_SHORT} BOARD_ID=${board} APP_ID=${APP_ID_SHORT}

        # Copy build artifacts out to build directory
        # Structure is /opt/openrov/firmware/BOARD/CPU/APP.bin
        mkdir -p ${BUILD_DIR}/${board}/${CPU_NAME}
        cp bin/${board}/${app}.hex ${BUILD_DIR}/${board}/${CPU_NAME}/${app}.bin
        cp bin/${board}/${app}.elf ${BUILD_DIR}/${board}/${CPU_NAME}/${app}.elf

        popd

        MANIFEST_FILE=${BUILD_DIR}/${board}/${CPU_NAME}/${app}.mani

        # Create a blank manifest
        rm ${MANIFEST_FILE} || true
        echo -e "git_hash:$GIT_HASH_SHORT" >> ${MANIFEST_FILE}
        echo -e "git_describe:$GIT_DESCRIBE" >> ${MANIFEST_FILE}

        # Add this build to the manifest file
        # jq --arg hash "$GIT_HASH_SHORT" '.["git_hash"] |= $hash' ${MANIFEST_FILE} | sponge ${MANIFEST_FILE}
        # jq --arg gitdescribe "$GIT_DESCRIBE" '.["git_describe"] |= $gitdescribe' ${MANIFEST_FILE} | sponge ${MANIFEST_FILE}
    done
done
