#pragma once

/**
 * Abstract class for both bno055 and bno088 drivers
 */


class BNOQuaternion 
{
public:        
    double& w()
    {
        return _w;
    }
    double& x()
    {
        return _x;
    }
    double& y()
    {
        return _y;
    }
    double& z()
    {
        return _z;
    }

    double w() const
    {
        return _w;
    }
    double x() const
    {
        return _x;
    }
    double y() const
    {
        return _y;
    }
    double z() const
    {
        return _z;
    }
    
    
protected:    
    double _w, _x, _y, _z;
    
};




class BNO {
    
protected:
    I2C*    dev;
    uint8_t slaveAddr;
    
public:   

    BNO(I2C* _dev, uint8_t _slaveAddr) :
        dev(_dev),
        slaveAddr(_slaveAddr)
    {}
    
    struct TRawIMUData
	{
		float x;
		float y;
		float z;
	};

	struct TCalibrationData
	{
		uint8_t mag;
		uint8_t accel;
		uint8_t gyro;
		uint8_t sys;
	};
            
    
public:    
    virtual i2c_retcode_t 			Reset( void ) = 0;
    virtual i2c_retcode_t 			Configure( void ) = 0;
    virtual i2c_retcode_t 			VerifyChipId( bool &validChip ) = 0;

    virtual i2c_retcode_t 			EnableAccel( void ) = 0;    
    virtual i2c_retcode_t			EnableRotationVector( void ) = 0;
    
    virtual i2c_retcode_t 			EnableGyro( void ) = 0;
    virtual i2c_retcode_t			EnableMagnetometer( void ) = 0;
            
    virtual i2c_retcode_t 			ReadQuaternion( BNOQuaternion &quatOut ) = 0;
    virtual i2c_retcode_t           ReadGyro( TRawIMUData &dataOut ) = 0;
    
    virtual bool					DataAvailable( void ) = 0;

    virtual void                    SetNewAngVelMsgAvailable(bool newQuatMsg ) = 0;
    virtual void                    SetNewQuatMsgAvailable(bool newAngVelMsg) = 0;
    
    virtual bool					GetNewQuatMsgAvailable() = 0;
    virtual bool					GetNewAngVelMsgAvailable() = 0;
    
    virtual float 					GetQuatI() = 0;
    virtual float 					GetQuatJ() = 0;

    virtual float 					GetQuatK() = 0;
    virtual float 					GetQuatReal() = 0;    
    
    virtual float 					GetGyroX() = 0;
    virtual float 					GetGyroY() = 0;
    virtual float 					GetGyroZ() = 0;
    
};

