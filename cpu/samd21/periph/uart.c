/*
 * Copyright (C) 2015 Freie Universität Berlin
 *
 * This file is subject to the terms and conditions of the GNU Lesser
 * General Public License v2.1. See the file LICENSE in the top level
 * directory for more details.
 */

/**
 * @ingroup     driver_periph
 * @{
 *
 * @file
 * @brief       Low-level UART driver implementation
 *
 * @author      Thomas Eichinger <thomas.eichinger@fu-berlin.de>
 * @author      Troels Hoffmeyer <troels.d.hoffmeyer@gmail.com>
 * @author      Hauke Petersen <hauke.petersen@fu-berlin.de>
 *
 * @}
 */

#include "cpu.h"
#include "sched.h"
#include "thread.h"

#include "periph/uart.h"
#include "periph/gpio.h"

// Allocate memory to store the callback functions for each UART interface
static uart_isr_ctx_t uart_ctx[ UART_NUMOF ];

static inline SercomUsart *_uart(uart_t dev)
{
    // Get the pointer to the base register of the given UART device
    return uart_config[dev].dev;
}

static int init_base( uart_t uart, uint32_t baudrate );

int uart_init( uart_t uart, uint32_t baudrate, uart_rx_cb_t rx_cb, void *arg )
{
    // Initialize basic functionality
    int res = init_base( uart, baudrate );

    if( res != 0 ) 
    {
        return res;
    }

    // Register receive callback and user data
    uart_ctx[uart].rx_cb    = rx_cb;
    uart_ctx[uart].arg      = arg;

    // Configure interrupts and enable RX interrupt
    _uart( uart )->INTENSET.reg = SERCOM_USART_INTENSET_RXC;
    NVIC_EnableIRQ( SERCOM0_IRQn + _sercom_id( _uart( uart ) ) );
    NVIC_SetPriority (SERCOM0_IRQn + _sercom_id( _uart( uart ) ), 0 );

    return 0;
}

static int init_base( uart_t uart, uint32_t baudrate )
{
    uint32_t baud;
    SercomUsart *dev;

    if( (unsigned int)uart >= UART_NUMOF ) 
    {
        return -1;
    }

    // Get the device's base register
    dev = _uart( uart );

    // Enable sync and async clocks
    uart_poweron( uart );

    // Configure GPIO pins
    gpio_init(uart_config[uart].rx_pin, GPIO_IN);
    gpio_init_mux(uart_config[uart].rx_pin, uart_config[uart].mux);

    gpio_init(uart_config[uart].tx_pin, GPIO_OUT);
    gpio_init_mux(uart_config[uart].tx_pin, uart_config[uart].mux);

    // Reset the UART device
    dev->CTRLA.reg = SERCOM_USART_CTRLA_SWRST;
    while (dev->SYNCBUSY.reg & SERCOM_USART_SYNCBUSY_SWRST) {}

    // Configure interface
    dev->CTRLA.reg = (SERCOM_USART_CTRLA_DORD |                             // Data order: LSB First
                      SERCOM_USART_CTRLA_SAMPR(0x1) |                       // 16x oversampling using fractional baud rate generation
                      SERCOM_USART_CTRLA_TXPO(uart_config[uart].tx_pad) |   // Transmit pinout
                      SERCOM_USART_CTRLA_RXPO(uart_config[uart].rx_pad) |   // Receive pinout
                      SERCOM_USART_CTRLA_MODE_USART_INT_CLK);               // Use internal clock

    // Calculate baudrate for asynchronous fractional baudrate generation
    baud = ((((uint32_t)CLOCK_CORECLOCK * 10) / baudrate) / 16);

    // Split into FP and BAUD
    dev->BAUD.FRAC.FP   = (baud % 10);
    dev->BAUD.FRAC.BAUD = (baud / 10);

    // Enable receiver and transmitter, use 1 stop bit
    dev->CTRLB.reg = ( SERCOM_USART_CTRLB_RXEN | SERCOM_USART_CTRLB_TXEN );
    while (dev->SYNCBUSY.reg & SERCOM_USART_SYNCBUSY_CTRLB) {}

    // Finally, enable the device
    dev->CTRLA.reg |= SERCOM_USART_CTRLA_ENABLE;

    return 0;
}

void uart_write(uart_t uart, const uint8_t *data, size_t len)
{
    for( size_t i = 0; i < len; i++ ) 
    {
        // Wait until data register is empty
        while( !( _uart(uart)->INTFLAG.reg & SERCOM_USART_INTFLAG_DRE ) ){}

        // Write byte to data register
        _uart(uart)->DATA.reg = data[i];
    }
}

void uart_poweron(uart_t uart)
{
    // Enable this interface in the power manager
    PM->APBCMASK.reg |= (PM_APBCMASK_SERCOM0 << _sercom_id(_uart(uart)));

    // Enable the clock for this interface
    GCLK->CLKCTRL.reg = ( GCLK_CLKCTRL_CLKEN 
                        | GCLK_CLKCTRL_GEN_GCLK0 
                        | (SERCOM0_GCLK_ID_CORE + _sercom_id(_uart(uart))) << GCLK_CLKCTRL_ID_Pos );
    while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) {}
}

void uart_poweroff(uart_t uart)
{
    // Disable this interface in the power manager
    PM->APBCMASK.reg &= ~(PM_APBCMASK_SERCOM0 << _sercom_id(_uart(uart)));

    // Disable the clock for this interface
    GCLK->CLKCTRL.reg = ((SERCOM0_GCLK_ID_CORE + _sercom_id(_uart(uart))) << GCLK_CLKCTRL_ID_Pos);
    while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) {}
}

static inline void irq_handler( int dev )
{
    SercomUsart *uart = _uart( dev );

    // Data Received
    if( uart->INTFLAG.reg & SERCOM_USART_INTFLAG_RXC ) 
    {
        // Call the associated receive callback for this interface
        // Note: The interrupt flag is cleared by reading the data register on this chip
        uart_ctx[ dev ].rx_cb( uart_ctx[ dev ].arg, (uint8_t)( uart->DATA.reg ) );
    }
    
    // Error
    if( uart->INTFLAG.reg & SERCOM_USART_INTFLAG_ERROR ) 
    {
        // Clear error flag
        uart->INTFLAG.reg = SERCOM_USART_INTFLAG_ERROR;
    }

    // Trigger scheduling if necessary
    if( sched_context_switch_request ) 
    {
        thread_yield_higher();
    }
}


// Set up ISRs for each interface
#ifdef UART_0_ISR
void UART_0_ISR(void)
{
    irq_handler(0);
}
#endif

#ifdef UART_1_ISR
void UART_1_ISR(void)
{
    irq_handler(1);
}
#endif

#ifdef UART_2_ISR
void UART_2_ISR(void)
{
    irq_handler(2);
}
#endif

#ifdef UART_3_ISR
void UART_3_ISR(void)
{
    irq_handler(3);
}
#endif

#ifdef UART_4_ISR
void UART_4_ISR(void)
{
    irq_handler(4);
}
#endif

#ifdef UART_5_ISR
void UART_5_ISR(void)
{
    irq_handler(5);
}
#endif
