#ifndef PERIPH_CONF_H_
#define PERIPH_CONF_H_

#include <stdint.h>

#include "cpu.h"
#include "periph_cpu.h"

#ifdef __cplusplus
extern "C" {
#endif

// Run at 48MHZ, using XOSC32K (1) or 8Mhz INTOSC (0)
#define CLOCK_USE_XOSC32_WITH_DFLL48    (1)
#define CLOCK_USE_PLL                   (0)

#if CLOCK_USE_XOSC32_WITH_DFLL48
    #define CLOCK_CORECLOCK (48000000ul)
    #define CLOCK_XOSC32K   (32768ul)

    // Constants for Clock generators
    #define GENERIC_CLOCK_GENERATOR_MAIN      (0u)
    #define GENERIC_CLOCK_GENERATOR_XOSC32K   (1u)
    #define GENERIC_CLOCK_GENERATOR_OSCULP32K (2u) /* Initialized at reset for WDT */
    #define GENERIC_CLOCK_GENERATOR_OSC8M     (3u)

    // Constants for Clock multiplexers
    #define GENERIC_CLOCK_MULTIPLEXER_DFLL48M (0u)

#elif CLOCK_USE_PLL
    // Edit these values to adjust the PLL output frequency
    #define CLOCK_PLL_MUL       (47U)   // Must be >= 31 & <= 95
    #define CLOCK_PLL_DIV       (1U)    // Adjust to your needs

    // Generate the actual used core clock frequency
    #define CLOCK_CORECLOCK     ( ( ( CLOCK_PLL_MUL + 1 ) * 1000000U ) / CLOCK_PLL_DIV )
#else
    // Edit this value to your needs
    #define CLOCK_DIV           (1U)

    // Generate the actual core clock frequency
    #define CLOCK_CORECLOCK     (8000000 / CLOCK_DIV)
#endif

// Configure Timer Peripherals
#define TIMER_NUMOF         (2U)
#define TIMER_0_EN          1
#define TIMER_1_EN          1

// Timer 0 configuration
#define TIMER_0_DEV         TC3->COUNT16
#define TIMER_0_CHANNELS    2
#define TIMER_0_MAX_VALUE   (0xffff)
#define TIMER_0_ISR         isr_tc3

// Timer 1 configuration
#define TIMER_1_DEV         TC4->COUNT32
#define TIMER_1_CHANNELS    2
#define TIMER_1_MAX_VALUE   (0xffffffff)
#define TIMER_1_ISR         isr_tc4

// UART Configuration
// TODO: Switch main and debug back when done developing
#define UART_DEV_MAIN       (UART_DEV(3))
#define UART_DEV_DEBUG      (UART_DEV(0))
#define UART_DEV_PORTMOTOR  (UART_DEV(4))
#define UART_DEV_VERTMOTOR  (UART_DEV(2))
#define UART_DEV_STARMOTOR  (UART_DEV(1))

static const uart_conf_t uart_config[] = 
{
    {
        &SERCOM0->USART,
        GPIO_PIN(PA,5),
        GPIO_PIN(PA,4),
        GPIO_MUX_D,
        UART_PAD_RX_1,
        UART_PAD_TX_0,
    },
    {
        &SERCOM4->USART,
        GPIO_PIN(PA,13),
        GPIO_PIN(PA,12),
        GPIO_MUX_D,
        UART_PAD_RX_1,
        UART_PAD_TX_0,
    },
    {
        &SERCOM1->USART,
        GPIO_PIN(PA,17),
        GPIO_PIN(PA,16),
        GPIO_MUX_C,
        UART_PAD_RX_1,
        UART_PAD_TX_0,
    },
    {
        &SERCOM3->USART,
        GPIO_PIN(PA,23),
        GPIO_PIN(PA,22),
        GPIO_MUX_C,
        UART_PAD_RX_1,
        UART_PAD_TX_0,
    },
    {
        &SERCOM5->USART,
        GPIO_PIN(PB,3),
        GPIO_PIN(PB,2),
        GPIO_MUX_D,
        UART_PAD_RX_1,
        UART_PAD_TX_0,
    }
};

// Interrupt function name mapping
#define UART_0_ISR          isr_sercom0
#define UART_1_ISR          isr_sercom4
#define UART_2_ISR          isr_sercom1
#define UART_3_ISR          isr_sercom3
#define UART_4_ISR          isr_sercom5

// UART Count
#define UART_NUMOF          (sizeof(uart_config) / sizeof(uart_config[0]))


// PWM Configuration
#define PWM_0_EN            1
#define PWM_MAX_CHANNELS    4

/* for compatibility with test application */
#define PWM_0_CHANNELS      PWM_MAX_CHANNELS

/* PWM device configuration */
static const pwm_conf_t pwm_config[] = 
{
#if PWM_0_EN
    {   TCC0, 
        {
            /* GPIO pin, MUX value, TCC channel */
            { GPIO_UNDEF, (gpio_mux_t)0, 0 },
            { GPIO_UNDEF, (gpio_mux_t)0, 1 },
            { GPIO_UNDEF, (gpio_mux_t)0, 2 },
            { GPIO_PIN(PA, 21), GPIO_MUX_F, 3 }
        }
    }
#endif
};

// PWM Device count
#define PWM_NUMOF           (1U)

// SPI Configuration
#define SPI_NUMOF           (0)

// I2C Configuration
#define I2C_NUMOF           (1U)
#define I2C_0_EN            1
#define I2C_IRQ_PRIO        1

#define I2C_0_DEV           SERCOM2->I2CM
#define I2C_0_IRQ           SERCOM2_IRQn
#define I2C_0_ISR           isr_sercom2

/* I2C 0 GCLK */
#define I2C_0_GCLK_ID       SERCOM2_GCLK_ID_CORE
#define I2C_0_GCLK_ID_SLOW  SERCOM2_GCLK_ID_SLOW

/* I2C 0 pin configuration */
#define I2C_0_SDA           GPIO_PIN(PA, 8)
#define I2C_0_SCL           GPIO_PIN(PA, 9)
#define I2C_0_MUX           GPIO_MUX_D

// RTC Configuration
#define RTC_NUMOF          (1U)
#define RTC_DEV            RTC->MODE2

// RTT Configuration
#define RTT_NUMOF          (1U)
#define RTT_DEV            RTC->MODE0
#define RTT_IRQ            RTC_IRQn
#define RTT_IRQ_PRIO       10
#define RTT_ISR            isr_rtc
#define RTT_MAX_VALUE      (0xffffffff)
#define RTT_FREQUENCY      (32768U)    /* in Hz. For changes see `rtt.c` */
#define RTT_RUNSTDBY       (1)         /* Keep RTT running in sleep states */

#ifdef __cplusplus
}
#endif

#endif
